#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'd4n1'
SITENAME = 'd4n1'
SITEURL = 'https://www.d4n1.org'
TAGLINE = 'Programming by coffee'

PATH = 'content'

TIMEZONE = 'America/Maceio'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Me', 'http://lattes.cnpq.br/3807423183790142'),)

# Social widget
SOCIAL = ()
    
TWITTER_USERNAME = '@4k4d4n1'

DEFAULT_PAGINATION = 8

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = 'theme'
