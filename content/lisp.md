title: Lisp
date: 2005-12-12
category: Language

Lisp (historically, LISP) is a family of computer programming
languages with a long history and a distinctive, fully parenthesized
prefix notation. Originally specified in 1958, Lisp is the
second-oldest high-level programming language in widespread use
today. Only Fortran is older, by one year. Lisp has changed since its
early days, and many dialects have existed over its history. Today,
the best known general-purpose Lisp dialects are Common Lisp and
Scheme.

#### Shell

```
lisp

print "Hello World"

[Lisp](https://www.gnu.org/software/gcl/)
