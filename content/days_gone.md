title: Days Gone
date: 2021-5-5
category: Game

Ride and fight into a deadly, post pandemic America. Play as Deacon St. John, a drifter and bounty hunter who rides the broken road, fighting to survive while searching for a reason to live in this open-world action-adventure game.

Fixing low FPS on DXVK.

#### Requirements

- Steam >= 1.0.0.68-1
- Bumblebee >= 3.2.1-27
- Proton Experimental
- Debian Sid :)

#### Init

```
wget https://github.com/doitsujin/dxvk/suites/2793793675/artifacts/62048006
unzip dxvk-master-c51080068e13f332d3373cfcedd981d22612ee70.zip
```

#### Install

```
cp x64/* ~/.steam/steam/steamapps/common/Proton - Experimental/files/lib64/wine/dxvk/
cp x32/* ~/.steam/steam/steamapps/common/Proton - Experimental/files/lib/wine/dxvk/
```

Run Steam and play Days Gone game :)

[d4n1](http://d4n1.org)
