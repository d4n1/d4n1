title: GnuNet
date: 2005-11-11
category: Net

GNUnet is a mesh routing layer for end-to-end encrypted
networking and a framework for distributed applications designed to
replace the old insecure Internet protocol stack.  In other words,
GNUnet provides a strong foundation of free software for a global,
distributed network that provides security and privacy. Along with an
application for secure publication of files, it has grown to include
all kinds of basic applications for the foundation of a GNU
internet.

[GNUnet](https://gnunet.org/)

