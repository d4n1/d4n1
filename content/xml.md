title: XML
date: 2004-11-11
category: Language

In computing, Extensible Markup Language (XML) is a markup
language that defines a set of rules for encoding documents in a
format that is both human-readable and machine-readable.

#### vim example.xml

```
<note>
    <to>RMS</to>
    <from>D4n1</from>
    <heading>Reminder</heading>
    <body>Happy Hacking!</body>
</note>
```

[XML](http://www.w3.org/TR/rec-xml)
