title: Org
date: 2017-11-11
category: Tool

Org mode is for keeping notes, maintaining TODO lists, planning
projects, and authoring documents with a fast and effective plain-text
system.

#### Run

```
vim init.org

#+TITLE: Init
#+AUTHOR: Daniel Pimentel <d4n1@d4n1.org>
#+DATE: 2017-04-04 sam.

* TODO GNU...
* TODO Guix...
* TODO Blog...
* DONE watch...
```

[Org](http://orgmode.org)
