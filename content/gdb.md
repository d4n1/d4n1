title: Gdb
date: 2019-12-12
category: Debug

GDB (GNU Project debugger) allows you to see what is going
on `inside' another program while it executes -- or what another
program was doing at the moment it crashed.

Start your program, specifying anything that might affect
its behavior;
- Make your program stop on specified conditions;
- Examine what has happened, when your program has stopped;
- Change things in your program, so you can experiment with correcting the effects of one bug and go on to learn about another.

#### Run

```
gdb foo
```

[GDB](http://www.gnu.org/software/gdb/)

