title: Bluez
date: 2009-11-11
category: Net

The Bluetooth wireless technology is a worldwide specification
for a small-form factor, low-cost radio solution that provides links
between mobile computers, mobile phones, other portable handheld
devices, and connectivity to the Internet. The specification is
developed, published and promoted by the Bluetooth Special Interest
Group (SIG).


[Bluez](http://www.bluez.org/)
