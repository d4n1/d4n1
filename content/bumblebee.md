title: Bumblebee
date: 2018-8-8
category: Graphic

Bumblebee aims to provide support for NVIDIA Optimus laptops for
GNU/Linux distributions. Using Bumblebee, you can use your NVIDIA card
for rendering graphics which will be displayed using the Intel card.

#### Install

```
apt install bumblebee primus
```

##### Usage

```
optirun glxgears -info
primusrun vim
optirun steam
```

#####  sudo nvim /etc/bumblebee/xorg.conf.nvidia

```
Section "ServerLayout"
    Identifier  "Layout0"
    Option      "AutoAddDevices" "true"
    Option      "AutoAddGPU" "false"
EndSection

Section "Device"
    Identifier  "DiscreteNvidia"
    Driver      "nvidia"
    VendorName  "NVIDIA Corporation"
    Option "ProbeAllGpus" "false"
    Option "NoLogo" "true"
    Option "UseEDID" "true"
    #Option "UseDisplayDevice" "none"
EndSection

Section "Screen"
    Identifier     "nvidia"
    Device         "DiscreteNvidia"
EndSection
```

[Bumblebee](http://bumblebee-project.org)

