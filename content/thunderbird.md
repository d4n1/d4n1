title: Thunderbird
date: 2004-1-1
category: Email

Mozilla Thunderbird is a free and open-source,[11] cross-platform email 
client, news client, RSS, and chat client developed by the Mozilla Foundation.
The project strategy was modeled after that of the Mozilla Firefox web 
browser. It is installed by default on Ubuntu desktop systems. 

#### Install

```
apt install thunderbird enigmail
```

[Thuderbird](https://www.thunderbird.net)
[Enigmail](https://www.enigmail.net)
