title: GPL
date: 2008-1-1
category: License

The GNU General Public License (GNU GPL or GPL) is a widely used
free software license, which guarantees end users the freedom to run,
study, share and modify the software. The license was originally
written by Richard Stallman of the Free Software Foundation (FSF) for
the GNU Project, and grants the recipients of a computer program the
rights of the Free Software Definition. The GPL is a copyleft license,
which means that derivative work can only be distributed under the
same license terms. GPL was the first copyleft license for general
use.

#### Freedoms

* The freedom to use the software for any purpose;
* the freedom to change the software to suit your needs;
* the freedom to share the software with your friends and neighbors;
* the freedom to share the changes you make.

[GNU GPL](https://www.gnu.org/licenses/gpl.html)

