title: Bash
date: 2000-5-5
category: Shell

Bash is the GNU Project's shell. Bash is the Bourne Again
SHell. Bash is an sh-compatible shell that incorporates useful
features from the Korn shell (ksh) and C shell (csh). It is intended
to conform to the IEEE POSIX P1003.2/ISO 9945.2 Shell and Tools
standard. It offers functional improvements over sh for both
programming and interactive use. In addition, most sh scripts can be
run by Bash without modification.

#### Kill shell locked

```
^\\
```

#### SSH locked

```
<ENTER>~.
```

#### Removing started file -

```
rm -- -file
```

#### Debuging segmentation fault

```
strace <PID>
```

#### Pausing the shell

```
^s
```

#### Alternating jobs

```
$<PID>
```

#### Re-run commands

```
!
```

#### Cleaning shell

```
^l
```

#### Last argument

```
!$
```

#### Back to previous directory

```
cd -
```

#### Find text in files recursively

```
grep -r 'text' .
```

#### Labels

```
^ - Control key
<ENTER> - Enter key
<PID> - Proccess ID
```

[GNU Bash](https://www.gnu.org/software/bash/)
