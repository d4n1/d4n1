title: Rsync
date: 2002-9-9
category: security

Rsync is  a fast and extraordinarily versatile file copying tool.  It can copy locally, to/from another host over any remote shell, or to/from a remote rsync daemon.  It offers a large number of  options  that  control every  aspect of its behavior and permit very flexible specification of the set of files to be copied.  It is famous for its delta-transfer algorithm, which reduces the amount of data sent over the  network  by  sending only  the  differences  between  the source files and the existing files in the destination.  Rsync is widely used for backups and mirroring and as an improved copy command for everyday use.


#### Install

```
sudo apt install rsync
```

#### Run

```
rsync <source> <username>@<host>:<path>
rsync -Cravzp <username>@<host>:<path> <destination>
rsync --port=<port> <source> <username>@<host>:<path>
```

[Rsync](https://rsync.samba.org/)
