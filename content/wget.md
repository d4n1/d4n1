title: Wget
date: 2001-9-9
category: Utils

GNU Wget is a free utility for non-interactive download of files
from the Web. It supports HTTP, HTTPS, and FTP protocols, as well as
retrieval through HTTP proxies.

#### Run

```
wget -r <url>
```

[GNU Wget](http://www.gnu.org/software/wget/)
