title: Mumble
date: 2009-5-5
category: Audio

Mumble uses a client–server architecture which allows users to
talk to each other via the same server. It has a very simple
administrative interface and features high sound quality and low
latency. All communication is encrypted to ensure user privacy.

[Mumble](https://wiki.mumble.info/wiki/Main_Page)
