title: OSI
date: 2000-12-12
category: Org

The Open Source Initiative (OSI) is a non-profit organization dedicated 
to promoting open-source software.

The organization was founded in late February 1998 by Bruce Perens and 
Eric S. Raymond, part of a group inspired by the Netscape Communications 
Corporation publishing the source code for its flagship Netscape 
Communicator product. 
Later, in August 1998, the organization added a board of directors.

Raymond was president from its founding until February 2005, followed 
briefly by Russ Nelson and then Michael Tiemann. 
In May 2012, the new board elected Simon Phipps as president and in 
May 2015 Allison Randal was elected as president when Phipps stepped 
down in preparation for the 2016 end of his Board term. 
Phipps became President again in September 2017.

The group adopted the Open Source Definition for open-source software, 
based on the Debian Free Software Guidelines. They also established 
the Open Source Initiative (OSI) as a steward organization for the movement.

[OSI](https://opensource.org)
