title: GnuPG
date: 2002-2-2
category: Crypt

GnuPG is a complete and free implementation of the OpenPGP
standard as defined by RFC4880 (also known as PGP). GnuPG allows to
encrypt and sign your data and communication, features a versatile key
management system as well as access modules for all kinds of public
key directories. GnuPG, also known as GPG, is a command line tool with
features for easy integration with other applications. GnuPG is Free
Software (meaning that it respects your freedom). It can be freely
used, modified and distributed under the terms of the GNU General
Public License.

####  Generating keys

```
gpg --full-gen-key
```

#### Listing keys

```
gpg --list-keys
```

#### Exporting a public key

```
gpg --armor --export user@domain.org > publickey
```

#### Exporting a private key

```
gpg --armor --export-secret-keys user@domain.org > publickey
```

#### Importing a public key

```
gpg --import friend.gpg
gpg --edit-key friend@domain.org
fpr
sign
check
```

#### Import a public key on GPG site

```
gpg --keyserver gpg.mit.ed --send-key <key>
gpg --keyserver pgp.mit.edu --search <email>
gpg --keyserver pgp.mit.edu --sign-key <key>
```

#### Encrypting and decrypting documents

```
gpg --output doc.gpg --encrypt --recipient friend@domain.org doc
gpg --output doc --descript doc.gpg
gpg --output doc.gpg --symmetric doc
```

#### Tips

If you have problems with pinentry program, try (overwrite
your correct path to pinentry program):

```
gpg-agent --daemon --use-standard-socket --pinentry-program /home/dani/.guix-profile/bin/pinentry-curses
```

[GnuPG](https://www.gnupg.org)
