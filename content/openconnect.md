title: OpenConnect
date: 2014-1-1
category: Net


OpenConnect is a cross-platform multi-protocol SSL VPN client which supports a lot of VPN protocols.


#### Install

```
sudo apt install openconnect
```

#### Usage

```
sudo openconnect --protocol=<protocol> -c <cert> -u <user> <server>
```

[OpenConnect](https://www.infradead.org/openconnect/)
