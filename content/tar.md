title: Tar
date: 2001-6-6
category: Utils

The tar program provides the ability to create tar archives, as
well as various other kinds of manipulation. For example, you can use
tar on previously created archives to extract files, to store
additional files, or to update or list files which were already
stored.

#### Compress

```
tar -cvf foo.tar file1 file2
```

#### Descompress

```
tar -xvf foo.tar
```

[GNU Tar](http://www.gnu.org/software/tar/)
