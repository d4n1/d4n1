title: Zsh
date: 2000-3-3
category: Shell

Zsh is a shell designed for interactive use, although it is also a powerful
scripting language. Many of the useful features of bash, ksh, and tcsh were
incorporated into zsh; many original features were added. 

#### Oh-my-zsh

```
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

#### nvim ~/.zshrc

```
export ZSH="/home/d4n1/.oh-my-zsh"

ZSH_THEME="minimal"

plugins=(
    debian
    git
    docker
)

source $ZSH/oh-my-zsh.sh

export DEFAULT_USER="d4n1"
export EDITOR="nvim"
export ELM_DISPLAY='wl'
export ELM_ACCEL='opengl'
export ECORE_EVAS_ENGINE='wayland_egl'
export ARCHFLAGS="-arch x86_64"
export EVAS_GL_NO_BLACKLIST=1
export XDG_DESKTOP_DIR="$HOME/Desktop"
export XDG_DOWNLOAD_DIR="$HOME/Desktop/download"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

alias python=python3
alias n=nvim

neofetch --color_blocks off
```

### n ~/.oh-my-zsh/themes/minimal.zsh-theme

```
ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[white]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[red]%}●%{$fg[white]%}]%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_CLEAN="]%{$reset_color%} "

ZSH_THEME_SVN_PROMPT_PREFIX="$ZSH_THEME_GIT_PROMPT_PREFIX"
ZSH_THEME_SVN_PROMPT_SUFFIX="$ZSH_THEME_GIT_PROMPT_SUFFIX"
ZSH_THEME_SVN_PROMPT_DIRTY="$ZSH_THEME_GIT_PROMPT_DIRTY"
ZSH_THEME_SVN_PROMPT_CLEAN="$ZSH_THEME_GIT_PROMPT_CLEAN"

ZSH_THEME_HG_PROMPT_PREFIX="$ZSH_THEME_GIT_PROMPT_PREFIX"
ZSH_THEME_HG_PROMPT_SUFFIX="$ZSH_THEME_GIT_PROMPT_SUFFIX"
ZSH_THEME_HG_PROMPT_DIRTY="$ZSH_THEME_GIT_PROMPT_DIRTY"
ZSH_THEME_HG_PROMPT_CLEAN="$ZSH_THEME_GIT_PROMPT_CLEAN"

vcs_status() {
  if (( ${+functions[in_svn]} )) && in_svn; then
    svn_prompt_info
  elif (( ${+functions[in_hg]} )) && in_hg; then
    hg_prompt_info
  else
    git_prompt_info
  fi
}

PROMPT='%2~ $(vcs_status)$%b '
```

#### nvim ~/.zprofile

```
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  enlightenment_start
fi
```

[ZSH](http://zsh.org/)

