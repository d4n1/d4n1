title: Ada
date: 2005-9-9
category: Language

Ada is a structured, statically typed, imperative,
wide-spectrum, and object-oriented high-level computer programming
language, extended from Pascal and other languages. It has built-in
language support for design-by-contract, extremely strong typing,
explicit concurrency, offering tasks, synchronous message passing,
protected objects, and non-determinism. Ada improves code safety and
maintainability by using the compiler to find errors in favor of
runtime errors. Ada is an international standard; the current
version (known as Ada 2012) is defined by ISO/IEC 8652:2012.


#### Install

```
sudo apt install gcc
```

#### vim example.adb

```
with Ada.Text_IO; use Ada.Text_IO;
procedure Hello is
begin
  Put_Line ("Hello, world!");
end Hello;
```

#### Run

```
gcc -c hello.adb
```

[Ada](http://www.adaic.org/)
