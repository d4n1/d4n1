title: Wayland
date: 2020-3-3
category: Lib

Wayland is intended as a simpler replacement for X, easier to
develop and maintain.
Wayland is a protocol for a compositor to talk to its clients as well
as a C library implementation of that protocol. The compositor can be
a standalone display server running on Linux kernel modesetting and
evdev input devices, an X application, or a wayland client itself. The
clients can be traditional applications, X servers (rootless or
fullscreen) or other display servers. Part of the Wayland project is
also the Weston reference implementation of a Wayland
compositor. Weston can run as an X client or under Linux KMS and ships
with a few demo clients. The Weston compositor is a minimal and fast
compositor and is suitable for many embedded and mobile use cases.

#### vim ~/.zshrc

```
export ECORE_EVAS_ENGINE='wayland_shm'
export ELM_DISPLAY='wl'
export ELM_ACCEL='none'

if [[ -z ${XDG_VTNR} ]]; then
    if [[ -z ${ DISPLAY} ]]; then
        enlightenment_start
        #startx
    fi
fi
```

[Wayland](https://wayland.freedesktop.org/)
