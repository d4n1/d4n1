title: Pascal
date: 2006-3-3
category: Language


The GNU Pascal Compiler (GPC) is, as the name says, the Pascal
compiler of the GNU family (GNU GCC).

[GNU Pascal](http://gnu.org/software/pascal)
