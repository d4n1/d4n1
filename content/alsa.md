title: Alsa
date: 2012-7-7
category: Audio

The Advanced Linux Sound Architecture (ALSA) provides audio and
MIDI functionality to the Linux operating system.

#### Run

```
alsamixer
alsactl store
```

[ALSA](http://www.alsa-project.org/)
