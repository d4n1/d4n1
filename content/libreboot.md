title: Libreboot
date: 2015-2-2
category: Bios

Libreboot is a free BIOS or UEFI replacement (free as in
freedom); libre boot firmware that initializes the hardware and starts
a bootloader for your operating system. It's also an open source BIOS,
but open source fails to promote freedom; please call libreboot free
software. Since 14 May 2016, Libreboot is part of the GNU project.

#### Install

```
./lenovobios_fistflash bin/YOURBOARD/YOURROM
reboot
./lenovobios_secondflash bin/YOURBOARD/YOURROM
```

[GNU Libreboot](https://libreboot.org/)
