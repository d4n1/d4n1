title: Serra do Rio do Rastro
date: 2021-12-12
category: Motorcycle

I ride ~4000 KM with Dark to Serra do Rio do Rastro in Santa Catarina, Brazil.

#### Trip

  - Honda CB500f 2018 (Dark)
  - ~8000 KM

![Dark and me](http://www.d4n1.org/theme/images/post/rio-rastro.jpg)

