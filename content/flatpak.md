title: Flatpak
date: 2022-4-4
category: Package

Flatpak is a utility for software deployment and package management for Linux. It is advertised as offering a sandbox environment in which users can run application software in isolation from the rest of the system. The [Flathub](https://flathub.org/home) is a App's repository.


#### Init

```
sudo apt install flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

#### Search

```
flatpak search <package>
```

#### Install

```
flatpak install <package>
```

#### List

```
flatpak list <package>
```

#### Remove

```
flatpak remove <package>
```

#### Run

```
flatpak run <package>
```

#### Sandbox

```
flatpak override <app> --filesystem=<host, home, path>
flatpak override <app> --nofilesystem=<host, home, path>
```

[Flatpak](https://flatpak.org/)

