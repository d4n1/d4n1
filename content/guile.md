title: Guile
date: 2006-4-4
category: Language

Guile is an implementation of the Scheme programming
language. Scheme (http://schemers.org/) is an elegant and conceptually
simple dialect of Lisp, originated by Guy Steele and Gerald Sussman,
and since evolved by the series of reports known as RnRS (the
Revised^n Reports on Scheme).

#### Shell

```
(+ 1 2 3) ; add some numbers
$1 = 6

(define (factorial n) ; define a function
(if (zero? n) 1 (* n (factorial (- n 1)))))

(factorial 20)

(getpwnam \"root\") ; look in /etc/passwd
```

#### Script
   
```
#!~.guix-profile/bin/guile -s
!#

(display \"Hello, world!\")
(newline)
```
 
[GNU Guile](http://www.gnu.org/software/guile/)
