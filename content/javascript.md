title: Javascript
date: 2005-7-7
category: Language

JavaScript is a high-level, dynamic, untyped, and interpreted
programming language. It has been standardized in the ECMAScript
language specification. Alongside HTML and CSS, JavaScript is one of
the three core technologies of World Wide Web content production; the
majority of websites employ it, and all modern Web browsers support it
without the need for plug-ins. JavaScript is prototype-based with
first-class functions, making it a multi-paradigm language, supporting
object-oriented, imperative, and functional programming styles. It has
an API for working with text, arrays, dates and regular expressions,
but does not include any I/O, such as networking, storage, or graphics
facilities, relying for these upon the host environment in which it is
embedded.

#### nvim example.js

```
document.write('Hello World');
```

#### nvim index.html

```
<html>
<body>
  <script language=\"JavaScript\" type=\"text/javascript\" src=\"example.js\">
</body>
</html>
```

[Javascript](https://developer.mozilla.org/en-US/docs/Web/javascript)
