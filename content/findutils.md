title: FindUtils
date: 2003-12-12
category: Utils

The GNU Find Utilities (Findutils) are the basic directory
searching utilities of the GNU operating system. These programs are
typically used in conjunction with other programs to provide modular
and powerful directory search and file locating capabilities to other
commands. Such as find, locale, updatedb and xargs.

#### Install

```
sudo apt install findutils
```

#### Run

```
find htdocs -name '*.html' -print0 | xargs -0 chmod a+r 
```

[GNU FindUtils](http://www.gnu.org/software/findutils/)
