title: Linux
date: 2003-8-8
category: Kernel

Linux is a family of open-source Unix-like operating systems based on the Linux kernel, an operating system kernel
first released on September 17, 1991, by Linus Torvalds. Linux is typically packaged in a Linux distribution.

[Linux](https://www.kernel.org/)
