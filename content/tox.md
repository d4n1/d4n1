title: Tox
date: 2016-6-6
category: Chat

Tox began a few years ago, in the wake of Edward Snowden's leaks
regarding NSA spying activity. The idea was to create an instant
messaging protocol that ran without any kind of central servers. The
system would be distributed, peer-to-peer, and encrypted end-to-end,
with no way to disable any of the encryption features; at the same
time, the protocol would be easily usable by the layperson with no
practical knowledge of cryptography or distributed systems. Work began
during the Summer of 2013 by a single anonymous developer (who
continues, to this day, to remain anonymous). This lone developer put
together a library implementing the Tox protocol. The library provides
all of the messaging and encryption facilities, and is completely
decoupled from any user-interface; for an end-user to make use of Tox,
they need a Tox client. Fast-forward a few years to today, and there
exist several independent Tox client projects, and the original Tox
core library implementation is nearing completion (in terms of
features). Tox (both core and clients) has thousands of users,
hundreds of contributors, and the project shows no sign of slowing
down. Recently, a group of some of the project's major contributors
have formed The Tox Project, an organization built around the
protection, promotion, and advancement of Tox and its development.

[Tox](https://tox.chat)
