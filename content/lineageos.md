title: LineageOS
date: 2016-8-8
category: OS

LineageOS, also known as LineageOS Android Distribution and
Lineage, is a free and open-source operating system for smartphones
and tablet computers, based on the Android mobile platform. It is the
successor to the highly popular custom ROM CyanogenMod, from which it
was forked in December 2016 when Cyanogen Inc. announced it was
discontinuing development and shut down the infrastructure behind the
project. Since Cyanogen Inc. retained the rights to the Cyanogen name,
the project rebranded its fork as LineageOS.

[LineageOs](https://www.lineageos.org/)
