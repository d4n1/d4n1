title: SCP
date: 2002-10-10
category: security

scp copies files between hosts on a network.  It uses ssh(1) for data transfer, and uses the same authentication and provides the same security as ssh(1).  scp will ask for passwords or passphrases if they are needed for authentication.

#### Install

```
sudo apt install scp
```

#### Run

```
scp [source] [username]@[host]:[path]
scp [username]@[host]:[path] [destination]
scp -P [port] [source] [username]@[host]:[path]
```

[SCP](https://www.openssh.com/)
