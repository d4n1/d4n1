title: Octave
date: 2015-5-5
category: Language


GNU Octave is a high-level interpreted language, primarily
intended for numerical computations. It provides capabilities for the
numerical solution of linear and nonlinear problems, and for
performing other numerical experiments. It also provides extensive
graphics capabilities for data visualization and manipulation. Octave
is normally used through its interactive command line interface, but
it can also be used to write non-interactive programs. The Octave
language is quite similar to Matlab so that most programs are easily
portable.

#### Run

```
octave
exp (i*pi)

[GNU Octave](http://gnu.org/software/octave/)
