title: Time
date: 2014-3-3
category: Utils

The 'time' command runs another program, then displays
information about the resources used by that program, collected by the
system while the program was running. You can select which information
is reported and the format in which it is shown, or have time save
the information in a file instead of displaying it on the screen.

#### Run

```
time program
```

[GNU Time](https://www.gnu.org/software/time/time.html)
