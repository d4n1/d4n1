title: Serra Corvo Branco
date: 2021-12-12
category: Motorcycle

I ride ~4000 KM with Dark to Serra do Corvo Branco in Santa Catarina, Brazil.

#### Trip

  - Honda CB500f 2018 (Dark)
  - ~8000 KM

![Dark in me](http://www.d4n1.org/theme/images/post/corvo-branco.jpg)

