title: Librejs
date: 2014-2-2
category: library

GNU LibreJS an add-on for GNU IceCat and Mozilla Firefox 
detects and blocks nonfree nontrivial JavaScript while allowing its
execution on pages containing code that is either trivial and/or
free.

[GNU Librejs](http://www.gnu.org/software/librejs/)
