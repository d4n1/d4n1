title: Gettext
date: 2001-11-11
category: Utils

This chapter explains the goals sought in the creation of GNU
gettext and the free Translation Project. Then, it explains a few
broad concepts around Native Language Support, and positions message
translation with regard to other aspects of national and cultural
variance, as they apply to programs. It also surveys those files used
to convey the translations. It explains how the various tools interact
in the initial generation of these files, and later, how the
maintenance cycle should usually operate.

#### The Format of PO Files

```
white-space
#  translator-comments
#. extracted-comments
#: reference…
#, flag…
#| msgid previous-untranslated-string
msgid untranslated-string
msgstr translated-string
```

[GNU Gettext](http://www.gnu.org/software/gettext/)
