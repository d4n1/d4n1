title: CFC
date: 2024-10-10
category: Motorcycle

CFC (Cardeal Fazedor de Chuva in portuguese, Cardinal Rainmaker in english) is a motorcycle challenge where the rider goes cardinal points of your country, Brazil.

#### Trip

- Honda CB 500F 2018 (Dark) and Yamaha FZ25 2023 (Rani)
- 13.090 km
- 66 days

#### Certificate

![CFC-AL](http://www.d4n1.org/theme/images/post/cfc.jpg)

Cardinal points of Brazil start in Chuí - Rio Grande do Sul (South) and follow from Pontas de Seixas - Paraíba (East), Mancio Lima - Acre (West) and Uiramutã - Roraima (North).

[VFC](http://www.fazedoresdechuva.com/forums/showthread.php/3341-Hacking-AL102)
