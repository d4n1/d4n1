title: JSON
date: 2004-12-12
category: Language

In computing, JSON (JavaScript Object Notation) is an
open-standard format that uses human-readable text to transmit data
objects consisting of attribute–value pairs. It is the most common
data format used for asynchronous browser/server communication,
largely replacing XML which is used by Ajax.

#### nvim example.json

```
{
  "firstName": "Daniel",
  "lastName": "Pimentel",
  "Style": "Geek"
}
```

[JSON](http://json.org)
