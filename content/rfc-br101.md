title: RFC BR-101
date: 2018-1-1
category: Motorcycle

RFC (Rodoviário Fazedor de Chuva in portuguese, Road Rainmaker in english) is a motorcycle challenge where the rider travels along the entire BR-101 highway in Brazil, called Translitorania's highway.

#### Trip

- Honda CB 500F 2018 (Dark)
- 4442 km
- 13 days

#### Route

![RFC-BR-101](http://www.d4n1.org/theme/images/post/rfc-br-101.jpg)

#### Certificate

![RFC-BR-101](http://www.d4n1.org/theme/images/post/rfc-br-101-cert.jpg)


[RFC](http://www.fazedoresdechuva.com/forums/showthread.php/3600-Hacking-BR)
