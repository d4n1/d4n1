title: Help2man
date: 2003-7-7
category: Man

help2man produces simple manual pages from the  `--help` and `--version` output of other commands

#### Usage

```
help2man [option]... executable
```

[Help2man](http://www.gnu.org/software/help2man/)
