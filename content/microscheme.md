title: Microscheme
date: 2015-3-3
category: Language

Microscheme, or (ms) for short, is a functional programming
language for the Arduino, and for Atmel 8-bit AVR microcontrollers in
general. Microscheme is a subset of Scheme, in the sense that every
valid (ms) program is also a valid Scheme program (with the exception
of Arduino hardware-specific primitives). The (ms) compiler performs
function inlining, and features an aggressive tree-shaker, eliminating
unused top-level definitions. Microscheme has a robust FFI (Foreign
Function Interface) meaning that C code may be invoked directly
from (ms) programs. Therefore, the power of the existing wealth of
Arduino libraries is available within Microscheme.

#### vim script.ms

```
;; Hello World in Microscheme!

(include 'libraries/lcd.ms')
(lcd-init)

(define foo 'Hi, ')
(define bar 'from Scheme!')

(for-each-vector write (vector-concat foo bar))
```

#### Compile

```
microscheme -u -m UNO -d /dev/ttyUSB0 script.ms
```

[Microscheme](http://microscheme.org/)
