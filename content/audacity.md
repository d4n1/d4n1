title: Audacity
date: 2002-12-12
category: Audio


Audacity is a free under GPL license, easy-to-use, multi-track
audio editor and recorder for Windows, Mac OS X, GNU/Linux and other
operating systems. Some features:

- Record live audio
- Convert tapes and records into digital recordings or CDs
- Edit WAV, AIFF, FLAC, MP2, MP3 or Ogg Vorbis sound files
- AC3, M4A/M4R (AAC), WMA and other formats supported using optional libraries
- Cut, copy, splice or mix sounds together
- Numerous effects including change the speed or pitch of a recording

#### Install

```
sudo apt install audacity
```

#### Run

```
audacity
```

[Audacity](http://www.audacityteam.org/)
