title: Debian
date: 2000-1-1
category: OS

The Debian Project is an association of individuals who have made
common cause to create a free operating system. This operating system
that we have created is called Debian.

An operating system is the set of basic programs and utilities that
make your computer run. At the core of an operating system is the
kernel. The kernel is the most fundamental program on the computer and
does all the basic housekeeping and lets you start other programs.

Debian systems currently use the Linux kernel or the FreeBSD
kernel. Linux is a piece of software started by Linus Torvalds and
supported by thousands of programmers worldwide. FreeBSD is an
operating system including a kernel and other software.

However, work is in progress to provide Debian for other kernels,
primarily for the Hurd. The Hurd is a collection of servers that run
on top of a microkernel (such as Mach) to implement different
features. The Hurd is free software produced by the GNU project.

A large part of the basic tools that fill out the operating system
come from the GNU project; hence the names: GNU/Linux, GNU/kFreeBSD,
and GNU/Hurd. These tools are also free.

Of course, the thing that people want is application software:
programs to help them get what they want to do done, from editing
documents to running a business to playing games to writing more
software. Debian comes with over 51000 packages (precompiled software
that is bundled up in a nice format for easy installation on your
machine), a package manager (APT), and other utilities that make it
possible to manage thousands of packages on thousands of computers as
easily as installing a single application. All of it free.

It's a bit like a tower. At the base is the kernel. On top of that are
all the basic tools. Next is all the software that you run on the
computer. At the top of the tower is Debian — carefully organizing and
fitting everything so it all works together.


#### Packaging

##### Install

```
# apt install debootstrap sbuild schroot blhc devscripts dh-make \
    git-buildpackage git sbuild-debian-developer-setup

# apt install autopkgtest blhc dput-ng how-can-i-help locales quilt \
    renameutils spell splitpatch gnupg tree wget neovim 
```


##### ~/.zshrc (me)

```
export ZSH="$HOME/.oh-my-zsh"

ZSH_THEME="lambda" # set by `omz`
plugins=(debian git docker python node)

source $ZSH/oh-my-zsh.sh

export DEFAULT_USER="d4n1"
export EDITOR="nvim"
export TERM="xterm-256color"
export DEBFULLNAME="Daniel"
export DEBEMAIL="d4n1@d4n1.org"
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8
export QUILT_PATCHES=debian/patches

export ELM_DISPLAY='wl'
export ELM_ACCEL='opengl'
export ECORE_EVAS_ENGINE='wayland_egl'
export ARCHFLAGS="-arch x86_64"
export EVAS_GL_NO_BLACKLIST=1

export XDG_DESKTOP_DIR="$HOME/Desktop"
export XDG_DOWNLOAD_DIR="$HOME/Desktop/download"

alias python="python3"
alias n="nvim"

neofetch --color_blocks off
```

##### ~/.gitconfig (me)

```
[user]
name = Daniel
email = d4n1@d4n1.org
```

##### /etc/devscripts.conf

```
DEBSIGN_KEYID="D9F9EADD6F26AC782CFCD8317FECE9042C7BFD45"
DEBUILD_DPKG_BUILDPACKAGE_OPTS="-us -uc -I -i"
DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"
```

##### /etc/lintianrc

```
display-info = yes
pedantic = yes
display-experimental = yes
color = auto
```


##### ~/.quitrc

```
display-info = yes
pedantic = yes
display-experimental = yes
color = auto
```

##### Sbuild

```
sbuild-debian-developer-setup
# sbuild-adduser $USER
cp /usr/share/doc/sbuild/examples/example.sbuildrc ~/.sbuildrc
```

###### ~/.sbuildrc

```
$build_arch_all = 1;
$distribution = 'unstable';
$build_source = 1;
$source_only_changes = 1;
$clean_source = 0;
$run_lintian = 1;
$lintian_opts = ['--info', '--display-info'];
```

##### ~/.gbp.conf 

```
[DEFAULT]
builder = sbuild
pristine-tar = True
```

##### Chroot

###### Init

```
# sh /usr/share/doc/sbuild/examples/sbuild-debian-developer-setup-update-all
```

###### Finish

```
schroot --end-session --all-sessions
```

##### Update Package

```
gbp clone git@salsa.debian.org:d4n1/[package].git
cd [package]
git remote add oficial git@salsa.debian.org:debian/[package].git

gbp buildpackage
gbp import-orig --uscan
gbp dch --commit

lintian -EvIL +pedantic

gbp buildpackage

gbp push --debian-tag=''
```

##### Patch-queue

```
gbp pq import 
git branch # patch-queue/master

nvim [file]
git add [file]
git commit -m '[message]'

gbp pq export
git brach # master
```

##### Quilt

```
quilt applied
quilt series
quit push -a

quilt new [package.patch]
quilt add [file]
nvim [file]
quilt refresh
quilt applied

quilt pop -a
```

##### Contribute

Open MR from your repo/branch to debian/master on package.

Open issues on Devel Team.


[Debian](https://www.debian.org)
