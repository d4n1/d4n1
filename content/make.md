title: Make
date: 2003-4-4
category: Compiler


GNU Make is a tool which controls the generation of executables
and other non-source files of a program from the program's source
files. Make gets its knowledge of how to build your program from a
file called the makefile, which lists each of the non-source files and
how to compute it from other files. When you write a program, you
should write a makefile for it, so that it is possible to use Make to
build and install the program.

#### vim makefile

```
CC      := gcc
CCFLAGS := 
LDFLAGS :=

TARGETS:= edit
MAINS  := $(addsuffix .o, $(TARGETS) )
OBJ    := kbd.o command.o display.o $(MAINS)
DEPS   := defs.h command.h

.PHONY: all clean

all: $(TARGETS)

clean:
	rm -f $(TARGETS) $(OBJ)

$(OBJ): %.o : %.c $(DEPS)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(TARGETS): % : $(filter-out $(MAINS), $(OBJ)) %.o
	$(CC) -o $@ $(LIBS) $^ $(CCFLAGS) $(LDFLAGS)
```

#### Run

```
make
```

[GNU Make](http://www.gnu.org/software/make/)
