title: Blender
date: 2003-11-11
category: Graphic

Blender is the free and open source 3D creation suite. It
supports the entirety of the 3D pipeline—modeling, rigging, animation,
simulation, rendering, compositing and motion tracking, even video
editing and game creation. Advanced users employ Blender’s API for
Python scripting to customize the application and write specialized
tools; often these are included in Blender’s future releases. Blender
is well suited to individuals and small studios who benefit from its
unified pipeline and responsive development process.

#### Install

```
sudo apt install blender
```

#### Run

```
blender
```

[Blender](https://www.blender.org)
