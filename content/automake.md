title: Automake
date: 2015-7-7
category: Compiler

Automake is a tool for automatically generating Makefile.ins
from files called Makefile.am. Each Makefile.am is basically a series
of make variable definitions1, with rules being thrown in
occasionally. The generated Makefile.ins are compliant with the GNU
Makefile standards.

#### nvim Makefile.am

```
SUBDIRS = src
dist_doc_DATA = README
```

#### nvim src/Makefile.am

```
bin_PROGRAMS = hello
hello_SOURCES = main.c
```

#### nvim configure.ac

```
AC_INIT([amhello], [1.0], [bug-automake@gnu.org])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])
AC_PROG_CC
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([
 Makefile
 src/Makefile
])
AC_OUTPUT
```

#### Run

```
autoreconf --install
```

[GNU Automake](http://gnu.org/software/automake)
