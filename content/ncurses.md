title: Ncurses
date: 2002-3-3
category: Lib


The ncurses (new curses) library is a free software emulation of
curses in System V Release 4.0 (SVr4), and more. It uses terminfo
format, supports pads and color and multiple highlights and forms
characters and function-key mapping, and has all the other SVr4-curses
enhancements over BSD curses. SVr4 curses is better known today as
X/Open Curses.

[GNU Ncurses](http://gnu.org/software/ncurses)
