title: Avahi
date: 2009-7-7
category: Net

Avahi is a free zero-configuration networking (zeroconf)
implementation, including a system for multicast DNS/DNS-SD service
discovery. It is licensed under the GNU Lesser General Public
License (LGPL).  Avahi is a system which enables programs to publish
and discover services and hosts running on a local network. For
example, a user can plug their computer into a network and have Avahi
automatically advertise the network services running on the machine
which could enable access to files and printers.

[Avahi](http://www.avahi.org/)

