title: HTML
date: 2004-9-9
category: Language

HyperText Markup Language (HTML) is the standard markup language
for creating web pages and web applications. With Cascading Style
Sheets (CSS), and JavaScript, it forms a triad of cornerstone
technologies for the World Wide Web. Web browsers receive HTML
documents from a webserver or from local storage and render them into
multimedia web pages. HTML describes the structure of a web page
semantically and originally included cues for the appearance of the
document.

#### nvim example.html

```
<!DOCTYPE html>
<html>
  <head>
    <title>This is a title</title>
  </head>
  <body>
    <p>Hello world!</p>
  </body>
</html>
```

[HTML](http://www.w3.org/html)
