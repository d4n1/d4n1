title: Irc
date: 2001-4-4
category: Net

Internet Relay Chat Protocol (IRCP) is an application layer
protocol that facilitates communication in the form of text. The chat
process works on a client/server networking model. IRC clients are
computer programs that a user can install on their system. These
clients communicate with chat servers to transfer messages to other
clients. IRC is mainly designed for group communication in
discussion forums, called channels, but also allows one-on-one
communication via private messages as well as chat and data
transfer, including file sharing.

[Freenode](https://freenode.net/)
