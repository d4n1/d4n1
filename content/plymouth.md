title: Plymouth
date: 2008-8-8
category: Package

Plymouth is a free application which provide bootsplash for Linux, which handles user interaction during the boot process. Plymouth supports animations using Direct Rendering Manager (DRM) and the KMS driver and is bundled into initrd which enables it to be launched before the file system is mounted. The program is named after Plymouth Rock, a name which symbolizes the program's role as a first point of entry for users to interact with a computer system.


#### Install

```
# apt install plymouth plymouth-themes
```

#### nvim /etc/default.grub

```
...
GRUB_GFXMODE=1920x1080
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

#### Run

```
# update-grub2
```

#### Theme

```
# plymouth-set-default-theme -l
# plymouth-set-default-theme -R <theme>
```

[Plymouth](https://www.freedesktop.org/wiki/Software/Plymouth/)

