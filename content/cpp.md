title: CPP
date: 2005-2-2
category: Language

C++ is a general-purpose programming language. It has
imperative, object-oriented and generic programming features, while
also providing facilities for low-level memory manipulation.

#### example.cpp

```
#include<iostream>

using namespace std;

int main()
{
    cout << "Hello World" << endl;
}
```

[C++](http://isocpp.org)
