title: LVM
date: 2015-6-6
category: Disk

Logical volume management provides a higher-level view of the disk storage on a computer system than the traditional view of disks and partitions. This gives the system administrator much more flexibility in allocating storage to applications and users.

Definitions:

- PV : Physical Volumes. This means the hard disk, hard disk partitions, RAID or LUNs from a SAN which form "Physical Volumes" (or PVs).
- VG : Volume Groups. This is a collection of one or more Physical Volumes.
- LV : Logical Volumes. LVs sit inside a Volume Group and form, in effect, a virtual partition.
- PE : Physical Extents. In order to manipulate the actual data, it is divided into blocks of data called Physical Extents.
- LE : Logical Extents. Similar to Physical Extents, but at the Logical Volume level. Physical Extents are to Physical Volumes as Logical Extents are to Logical Volumes. The size of blocks are the same for each logical volume (LV) in the same volume group (VG).

#### Views

```
# vgdisplay
# lvdisplay
```

#### Extend

```
# lvextend -l +100%FREE /dev/<vg>/<lv>
# resize2fs /dev/mapper/<vg>
```

#### Resize

Resize target partition with free space and write it with cfdisk or another.

```
# cfdisk /dev/<disk>
# pvresize /dev/<disk>
# lventend -l +100%FREE /dev/<vg>/<lv>
# resize2fs /dev/mapper/<vg>
```

[LVM](https://translate.google.com/website?sl=en&tl=pt&hl=pt&client=srp&u=https://sourceware.org/lvm2/)
