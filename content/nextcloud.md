title: Nextcloud
date: 2017-5-5
category: Cloud

Nextcloud is a suite of client-server software for creating and using file hosting services. Nextcloud is
free and open-source, which means that anyone is allowed to install and operate it on their own private 
server devices.

#### Run

```
docker run -d -p 8080:80 nextcloud
```

[Nextcloud](https://nextcloud.com/)
