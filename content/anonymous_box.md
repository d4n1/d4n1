title: Anonymous' Box
date: 2021-10-10
category: Proxy

Anonymous' Box use Tor and Raspberry Pi.
Tor is open source project for enabling anonymous communication.
Raspberry Pi is a single-board computer.

#### Requirements

- Raspberry Pi >= 3 
- MicroSD card

#### Init

```
wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-01-12/2021-01-11-raspios-buster-armhf-lite.zip
sudo dd if=2021-01-11-raspios-buster-armhf-lite.img of=/dev/sdb bs=1MB
```

#### Install

```
sudo raspi-config
sudo apt update
sudo apt upgrade
sudo apt install tor
```

#### sudo nvim /etc/network/interfaces

```
allow-hotplug eth0
iface eth0 inet static
address 192.168.8.42
netmask 255.255.255.0
gateway 192.168.8.1
```

#### sudo nvim /etc/tor/torrc

```
SocksPort 192.168.8.42:8042
SocksPolicy accept 192.168.8.0/24
RunAsDaemon 1
DataDirectory /var/lib/tor
```

#### Restart

```
sudo systemctl restart tor@default.service
```

Configure your browser with Tor proxy and use anonymous mode :)


[d4n1](http://d4n1.org)
