title: Replicant
date: 2016-9-9
category: OS


Replicant is a fully free Android distribution running on
several devices, a free software mobile operating system putting the
emphasis on freedom and privacy/security. It is based on CyanogenMod
and replaces or avoids every proprietary component of the system, such
as user-space programs and libraries as well as firmwares.

[Replicant](http://www.replicant.us/)
