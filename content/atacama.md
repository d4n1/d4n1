title: Atacama
date: 2023-6-6
category: Motorcycle

I ride 13000 KM with Funérea (MT-07) from Maceió in Brazil to Atacama in Chile.

#### Trip

  - Yamaha MT-07 2016 (Funérea)
  - 13000 KM

![Funérea on Atacama](http://www.d4n1.org/theme/images/post/atacama.jpg)

