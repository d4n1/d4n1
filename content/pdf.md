title: PDF
date: 2003-10-10
category: Lib

The goal of the GNU PDF Suite project is to develop and provide
a free, high-quality and fully functional set of libraries and
programs that implement the PDF file format.

[GNU PDF](http://www.gnu.org/software/pdf/)
