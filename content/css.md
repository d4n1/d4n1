title: CSS
date: 2004-10-10
category: Language

Cascading Style Sheets (CSS) is a style sheet language used for
describing the presentation of a document written in a markup
language. Although most often used to set the visual style of web
pages and user interfaces written in HTML and XHTML, the language can
be applied to any XML document, including plain XML, SVG and XUL, and
is applicable to rendering in speech, or on other media. Along with
HTML and JavaScript, CSS is a cornerstone technology used by most
websites to create visually engaging webpages, user interfaces for web
applications, and user interfaces for many mobile applications.

#### vim example.css

```
p {
    text-align: center;
    color: red;
}
```

#### vim index.html

```
<!DOCTYPE html>
<html>
  <head>
    <link href=\"example.css\" rel=\"stylesheet\">
  </head>
<body>
  <p>Every paragraph will be affected by the style.</p>
</body>
</html>
```

[CSS](http://www.w3.org/Style/CSS)
