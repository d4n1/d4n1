title: Anarchism
date: 2020-4-4
category: Political


Exhaustive exploration of Anarchist theory and practice

Anarchism is a political theory which aims to create anarchy,
"the absence of a master, of a sovereign." In other words,
anarchism is a political theory which aims to create a society
within which individuals freely co-operate together as equals.
As such anarchism opposes all forms of hierarchical control -
be that control by the state or a capitalist - as harmful to
the individual and their individuality as well as unnecessary.

#### Init

```
sudo apt install anarchism
```

[Anarchism](http://www.anarchismfaq.org/)
