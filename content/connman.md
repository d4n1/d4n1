title: ConnMan
date: 2008-5-5
category: Net

ConnMan is an internet connection manager for embedded devices running the Linux operating system.

#### Install

```
apt install connman-ui connman-vpn connman-dev
```

#### Usage

```
connmanctl scan wifi
connmanctl services
connmanctl connect <wifi>
```

#### Connecting to a protected access point

```
connmanctl
scan wifi
services
agent on
connect <wifi-psk>
quit
```

#### Tips

Avoid ConnMan manager Docker bridges.

```
nvim /etc/connman/main.conf

NetworkInterfaceBlacklist = vmnet,vboxnet,virbr,ifb,ve-,vb-,veth
```

[ConnMan](https://git.kernel.org/pub/scm/network/connman/connman.git)

