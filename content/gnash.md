title: Gnash
date: 2001-3-3
category: Graphic

GNU Gnash is the GNU Flash movie player — Flash is an animation
file format pioneered by Macromedia which continues to be supported by
their successor company, Adobe. Flash has been extended to include
audio and video content, and programs written in ActionScript, an
ECMAScript-compatible language. Gnash is based on GameSWF, and
supports most SWF v7 features and some SWF v8 and v9.

#### Shell

```
gnash foo.swf
```

[GNU Gnash](http://www.gnu.org/software/gnash/)
