title: Hurd
date: 2003-9-9
category: kernel

The GNU Hurd is the GNU project's replacement for the Unix
kernel. It is a collection of servers that run on the Mach microkernel
to implement file systems, network protocols, file access control, and
other features that are implemented by the Unix kernel or similar
kernels (such as Linux).

#### Mach

Mach is a so-called first generation microkernel. It is the
microkernel currently used by the Hurd.

#### Mig

The Mach Interface Generator (MIG) is an IDL compiler. Based on
an interface definition, it creates stub code to ?invoke object
methods and to demultiplex incoming messages. These stub functions
conveniently hide the details of Mach's IPC and port machinery and
make it easy to implement and use Mach ?interfaces as remote procedure
calls (RPC): by using the stub functions, the client programs can call
remote procedures more or less like any other C function.

#### Development

The GNU Hurd is under active development. Because of that, there
is no stable version. The latest release is GNU Hurd 0.8.

[GNU Hurd](https://www.gnu.org/software/hurd/")
