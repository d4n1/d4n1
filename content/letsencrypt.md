title: Let's Encrypt
date: 2019-7-7
category: Crypt

Let’s Encrypt is a free, automated, and open certificate authority (CA), 
run for the public’s benefit. It is a service provided by the Internet 
Security Research Group (ISRG).

#### Install

```
sudo apt install certbot python-certbot-nginx 
```

#### Nginx

```
sudo certbot --nginx
```

#### Renew

```
sudo certbot renew --dry-run
```

[Let's Encrypt](https://letsencrypt.org/)
