title: CVS
date: 2002-5-5
category: CVS

CVS is a version control system, an important component of
Source Configuration Management (SCM). Using it, you can record the
history of sources files, and documents. It fills a similar role to
the free software RCS, PRCS, and Aegis packages.


#### Init

```
sudo apt install cvs
```

#### Run

```
touch foo
cvs add foo

cvs commit -m 'commit message'
cvs update

cvs diff foo bar

cvs tag development

rm foo
cvs delete foo
```

[CVS](http://cvs.nongnu.org/)
~
