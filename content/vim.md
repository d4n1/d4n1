title: Vim
date: 2000-6-6
category: Editor

Vim is a highly configurable text editor built to enable efficient text 
editing. 
It is an improved version of the vi editor distributed with most UNIX systems.

Vim is often called a "programmer's editor," and so useful for programming 
that many consider it an entire IDE. 
It's not just for programmers, though. 
Vim is perfect for all kinds of text editing, from composing email to 
editing configuration files.

Despite what the above comic suggests, Vim can be configured to work in 
a very simple (Notepad-like) way, called evim or Easy Vim.


#### Install

```
apt install vim
```

#### Plug

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

#### Custom

```
vim ~/.vimrc

" Plugins
call plug#begin("~/.vim/plugged")
  Plug 'joshdick/onedark.vim'
  Plug 'itchyny/lightline.vim'
  Plug 'preservim/nerdtree'
  Plug 'Xuyuanp/nerdtree-git-plugin'
  Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
  Plug 'PhilRunninger/nerdtree-visual-selection'
  Plug 'ryanoasis/vim-devicons'
  Plug 'ap/vim-buftabline'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'qpkorr/vim-bufkill'
  Plug 'Yggdroot/indentLine'
  Plug 'prettier/vim-prettier'
  Plug 'tpope/vim-fugitive'
  Plug 'sheerun/vim-polyglot'
  Plug 'dense-analysis/ale'
  Plug 'jiangmiao/auto-pairs'
  Plug 'mattn/emmet-vim'
  Plug 'ycm-core/YouCompleteMe'
call plug#end()

" Global
syntax on
syntax enable
filetype plugin on
set encoding=UTF-8
set textwidth=79
set tabstop=2
set shiftwidth=2
set softtabstop=2
set autoindent
set expandtab
set smarttab
set laststatus=2
set ignorecase
set t_Co=256
set noshowmode
set cursorline
set number
set hlsearch
set hidden
set nobuflisted

" Theme
colorscheme onedark
let g:lightline = { 'colorscheme': 'onedark' }

" Ale
let g:ale_completion_enabled = 1
set omnifunc=ale#completion#OmniFunc

" YouCompleteMe
let g:ycm_key_list_select_completion = ['<TAB>', '<Down>', '<Enter>']

" Nerdtree
nmap <C-e> :NERDTreeToggle<CR>
nmap <C-r> :NERDTreeRefreshRoot<CR>
autocmd BufRead * call SyncTree()

function! IsNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

function! SyncTree()
  if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
  ¦ NERDTreeFind
  ¦ wincmd p
  endif
endfunction

" Buftabline
nmap <C-f> :bnext<CR>
nmap <C-p> :bprev<CR>

" BufKill
nmap <C-c> :BD<CR>

" CtrlP
let g:ctrlp_map = '<c-s>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_user_command = {
\    'types': {
\      1: [
\        '.git',
\        'cd %s &&
\         git ls-files . -co --exclude-standard
\         | awk ''{ print length, $0 }''
\         | sort -n -s
\         | cut -d" " -f2-'
\      ],
\    },
\    'fallback': 'find %s -type f'
\  }

" Emmet
let g:user_emmet_leader_key = '<C-n>'

" Misc
if exists('+autochdir')
  ¦ set autochdir
else
  ¦ autocmd BufEnter * silent! lcd %:p:h:gs/ /\\ /
endif
```

#### Autocomplete

```
python3 ~/.vim/plugged/YouCompleteMe/install.py --all

```

#### Vim Script

```
echo "Hello, world!"

let i = 1
while i < 5
  echo "count is" i
  let i += 1
endwhile
unlet i
```

#### Shortcuts

##### General

```
:q        close
:w        write/saves
:wa[!]    write/save all windows [force]
:wq       write/save and close
:x        save and quit, same as wq
:q!       force close if file has changed and not save changes
```

```
v        Enter visual mode for selection of LINES
C-v      Enter visual mode for selection of BLOCKS
y        Yank/copy selected region
yy       Yank/copy entire line
"<reg>y  Yank/copy marked region into register <reg> (register from a-z)
c        Cut selection
p        Paste yanked content
"<reg>p  Paste yanked content in register <reg> (from a-z)
P        Paste yanked content BEFORE
```

```
u        Undo
C-r      Redo
```

```
:!<cmd>  Execute shell command <cmd>
C-z      send vim to background (fg brings it to front again)
```

```
C-ws     Split current window horizontally (alternative :split)
C-wv     Split current window vertically (alternative :vsplit)
C-ww     Jump to the next window
C-wo     Only one window (close all)
C-wARROW Jump to window left/right/top/bottom (arrow keys) to the current
C-w#<    Shrink/resize current window from the right by # (default 1)
C-w#>    Increase/resize current window to the right by # (default 1)
```

```
a        Append text after the cursor
A        Append text at the end of the line
i        Insert text before the cursor
I        Insert text before the first non-blank in the line
o        Begin a new line BELOW the cursor and insert text
O        Begin a new line ABOVE the cursor and insert text
s        Erase the current letter under the cursor, set insert-mode
S        Erase the whole line, set insert-mode
cc       Delete the current line, set insert-mode
cw       Delete word, set insert-mode
dd       Delete line under curser
```

```
q[a-z]   Start recording, everything will be recorded including movement actions.
@[a-z]   Execute the recorded actions.
```

```
nnoremap <silent> <leader>s :set spell!<cr>
```

```
<leader>s Toggle Spelling
]s       Next spelling mistake
[s       Previous spelling mistake
z=       Give Suggestions (prepent 1, use first suggestions automatically)
zg       Add misspelled to spellfile
zug      Remove word from spellfile
```

##### Navigation

```
h        cursor left
j        cursor down
l        cursor right
k        cursor up
```

```
H        Jump to TOP of screen
M        Jump to MIDDLE of screen
L        Jump to BOTTOM of screen
C-b      Move back one full screen (page up)
C-f      Move forward one full screen (page down)
C-d      Move forward 1/2 screen; half page down
C-u      Move back (up) 1/2 screen; half page up
```

```
w        jump by start of words (punctuation considered words)
e        jump to end of words (punctuation considered words)
b        jump backward by words (punctuation considered words)
0 (zero) start of line
^        first non-blank character of line
$        end of line
G        bottom of file
gg       top of file
```

```
E        jump to end of words (no punctuation)
W        jump by words (spaces separate words)
B        jump backward by words (no punctuation)
#G       goto line #
#gg      goto line #
```

##### Search

```
*        search for word under cursor (forward) and highlight occurrence (see incsearch, hlsearch below)
%        jump from open/close ( / #if / ( / { to corresponding ) / #endif / }
[{       jump to start of current code block
]}       jump to end of current code block
gd       jump to var declaration (see incsearch, hlsearch below)
f<c>     Find char <c> from current cursor position -- forwards
F<c>     Find char <c> from current cursor position -- backwards
,        Repeat previous f<c> or F<c> in opposite direction
;        Repeat previous f<c> or F<c> in same direction
'.       jump back to last edited line.
g;       jump back to last edited position.
[m       jump to start of funtion body
[i       show first declartion/use of the word under cursor
[I       show all occurrences of word under cursor in current file
[/       cursor to N previous start of a C comment
```

```
:vimgrep /<regex>/g %        Search for <regex> with multiple occasions per line (g)
                             in current file (%)
:vimgrep /<C-r>// %          On the command line, <C-r>/ (that is: CTRL-R followed by /)
                             will insert the last search pattern.
:vimgrep /<a>/g <filelist>   Search in the given files (<filelist>)
:vimgrep /<a>/g *.cc         Search in all *.cc files current directory
:vimgrep /<a>/g **/*.cc      Search in all *.cc files in every sub-directory (recursively)
:vimgrep /<a>/g `find . -type f`
                             Search in all files that are returns by the backtick command.

:vim     short for :vimgrep

:cnext   Jump to next record/match in quickfix list
:cprev   Jump to previous record/match in quickfix list
```

```
[q       see :cprev
]q       see :cnext
[Q       see :cfirst
]Q       see :clast
```

##### Marks

```
ma       set mark a at current cursor location
'a       jump to line of mark a (first non-blank character in line)
`a       jump to position (line and column) of mark a
d'a      delete from current line to line of mark a
d`a      delete from current cursor position to position of mark a
c'a      change text from current line to line of mark a
y`a      yank text to unnamed buffer from cursor to position of mark a
:marks   list all the current marks
:marks aB list marks a, B
```

##### Editing

```
x        Delete char UNDER cursor
X        Delete char BEFORE cursor
#x       Delete the next # chars. starting from char under cursor
dw       Delete next word
dW       Delete UP TO the next word
d^       Delete up unto the beginning of the line
d$       Delete until end of the line
D        See d$, delete until end of the line
dd       Delete whole line
dib      Delete contents in parenthesis '(' ')' block (e.g. function args)
diB      Delete inner '{' '}' block
daB      Delete a '{' '}' block
das      Delete a senctence
diw      Delete word under cursor
df<c>    Delete until next occurence of <c> (char) found (including <c>) [in single line]
dt<c>    Delete until next occurence of <c> (char) found (without <c>!!!) [in single line]

ciw      Change word under cursor
ciB      Change inner '{' '}' block
cf<c>    See "df<c>" but change instead of delete
ct<c>    See "dt<c>" but change instead of delete

#J       Merge # number of lines together
```

##### Misc

```
gq       (in visual-mode) format selected text according to line-width
gqq      format current line according to line-width
#gqq     format next #-lines
```

```
C-n      Keyword completion
Tab      Keyword completion (SuperTab plugin)
r<c>     Replace char <c>
#r<c>    Replace follow # chars with <c>, : csock, cursor on s, 3re ceeek
```

```
:s/xxx/yyy/    Replace xxx with yyy at the first occurrence
:s/xxx/yyy/g   Replace xxx with yyy first occurrence, global (whole sentence)
:s/xxx/yyy/gc  Replace xxx with yyy global with confirm
:%s/xxx/yyy/g  Replace xxx with yyy global in the whole file
```

```
u        Convert selection (visual mode) to lowercase
U        Convert selection (visual mode) to uppercase
```

```
:g/^#/d  Delete all lines that begins with #
:g/^$/d  Delete all lines that are empty
```

```
gg=G Reindent all
```

[Vim](https://www.vim.org)
