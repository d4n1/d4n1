title: Rust
date: 2012-2-2
category: Language

Rust is a systems programming language that runs blazingly fast,
prevents segfaults, and guarantees thread safety.

#### vim main.rs

```
use ferris_says::say; // from the previous step
use std::io::{stdout, BufWriter};

fn main() {
    let stdout = stdout();
    let message = String::from("Hello fellow Rustaceans!");
    let width = message.chars().count();

    let mut writer = BufWriter::new(stdout.lock());
    say(message.as_bytes(), width, &mut writer).unwrap();
}
```

#### Run

```
cargo run
```

[Rust](https://www.rust-lang.org/)
