title: Linux-libre
date: 2002-01-01
category: kernel

Linux, the kernel developed and distributed by Linus Torvalds et
al, contains non-Free Software, i.e., software that does not respect
your essential freedoms, and it induces you to install additional
non-Free Software that it doesn't contain.

#### Free as in Freedo

GNU Linux-libre is a project to maintain and publish 100% Free
distributions of Linux, suitable for use in Free System Distributions,
removing software that is included without source code, with
obfuscated or obscured source code, under non-Free Software licenses,
that do not permit you to change the software so that it does what you
wish, and that induces or requires you to install additional pieces of
non-Free Software.

[Linux Libre](http://www.fsfla.org/ikiwiki/selibre/linux-libre/index.en.html)
