title: Magisk
date: 2021-9-9
category: Mobile

Magisk is an all-in-one root solution for Android devices.

#### Install

Install by TWRP using Magisk zipfile.


#### Dependencies

- Busybox
- Debloater
- Termux

#### Termux

```
su
debloat
```

[Magisk](https://magisk.me/)
