title: Cups
date: 2012-5-5
category: Net

CUPS is the standards-based, open source printing system
developed by Apple Inc. for macOS® and other UNIX®-like operating
systems. CUPS uses the Internet Printing Protocol (IPP) to support
printing to local and network printers."

[CUPS](https://www.cups.org/)
