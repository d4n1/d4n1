title: OpenVPN
date: 2014-12-12
category: Net


OpenVPN is a virtual private network (VPN) system that implements techniques
to create secure point-to-point or site-to-site connections in routed or 
bridged configurations and remote access facilities. It implements both
client and server applications.


#### Install

```
sudo apt install openvpn
```

#### Usage

```
sudo openvpn --client --config <file.ovpn>
```

[OpenVPN](https://openvpn.net/)
