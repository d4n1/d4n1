title: Gawk
date: 2002-4-4
category: Language

The GNU implementation of awk is called gawk; if you invoke it
with the proper options or environment variables, it is fully
compatible with the POSIX1 specification of the awk language and with
the Unix version of awk maintained by Brian Kernighan. This means that
all properly written awk programs should work with gawk. So most of
the time, we don’t distinguish between gawk and other awk
implementations.

#### Install

```
sudo apt install gawk
```

#### Run

```
awk 'program' input-file1 input-file2
```

[GNU Gawk](http://gnu.org/software/gawk/)
