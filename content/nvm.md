title: NVM
date: 2021-8-8
category: Develop

NVM (Node Virtual Machine) is a version manager for node.js, designed 
to be installed per-user, and invoked per-shell

#### Install

```
sudo apt install curl wget tar
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
source ~/.zshrc
```

#### Usage

```
nvm ls-remote
nvm install x.y.z
nvm use y.y.z
nvm alias default x.y.z
nvm install node
nvm ls
```

[NVM](https://github.com/nvm-sh/nvm)
