title: Grub
date: 2001-2-2
category: Boot

GNU GRUB is a Multiboot boot loader. It was derived from GRUB,
the GRand Unified Bootloader, which was originally designed and
implemented by Erich Stefan Boleyn. Briefly, a boot loader is the
first software program that runs when a computer starts. It is
responsible for loading and transferring control to the operating
system kernel software (such as the Hurd or Linux). The kernel, in
turn, initializes the rest of the operating system (e.g. GNU).

#### sudo nvim /etc/default/grub

```
GRUB_DEFAULT=0
GRUB_TIMEOUT=4
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pci=noaer"
GRUB_CMDLINE_LINUX=""
GRUB_BACKGROUND="/boot/grub/splash.jpg"
```

#### Update

```
sudo apt install imagemagick
cd ~/Desktop/media/image/wallpaper
gimp splash.jpg
sudo convert -resize 640x480 -colors 14 splash.jpg splash.jpg
sudo cp splash.jpg /boot/grub/
sudo update-grub2
```

[GNU Grub](https://www.gnu.org/software/grub/)
