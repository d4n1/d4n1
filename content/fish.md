title: Fish
date: 2020-1-1
category: Shell

The friendly interactive shell (fish) is a Unix shell that attempts to be more
interactive and user-friendly than those with a longer history or those 
formulated as function-compatible replacements for the aforementioned.
The design goal of fish is to give the user a rich set of powerful
features in a way that is easy to discover, remember, and use. Fish is
considered an "exotic shell", in that its syntax derives from neither
the Bourne shell (ksh, Bash, zsh) nor the C shell (csh, tcsh).
Also unlike previous shells, which disable certain features by
default to save system resources, fish enables all features by default. 

#### Install

```
sudo apt install fish
```

#### Oh-my-fish

```
curl -L https://get.oh-my.fish | fish

omf install <theme>
```

#### vim ~/.config/fish/config.fish

```
set fish_greeting

set -U fish_color_normal normal
set -U fish_color_command ffffff
set -U fish_color_quote a8a8a8
set -U fish_color_redirection 808080
set -U fish_color_end 949494
set -U fish_color_error 585858
set -U fish_color_param d7d7d7
set -U fish_color_selection white --bold --background=brblack
set -U fish_color_search_match bryellow --background=brblack
set -U fish_color_history_current --bold
set -U fish_color_operator 00a6b2
set -U fish_color_escape 00a6b2
set -U fish_color_cwd green
set -U fish_color_cwd_root red
set -U fish_color_valid_path --underline
set -U fish_color_autosuggestion 777777
set -U fish_color_user brgreen
set -U fish_color_host normal
set -U fish_color_cancel -r
set -U fish_pager_color_completion normal
set -U fish_pager_color_description B3A06D yellow
set -U fish_pager_color_prefix white --bold --underline
set -U fish_pager_color_progress brwhite --background=cyan
set -U fish_color_comment bcbcbc
set -U fish_color_match --background=brblue

set -U ECORE_EVAS_ENGINE wayland_egl
set -U ELM_DISPLAY wl
set -U ELM_ACCEL opengl
set -U TERM xterm-256color
set -U ARCHFLAGS -arch x86_64
set -U EVAS_GL_NO_BLACKLIST 1
set -U XDG_DESKTOP_DIR $HOME/Desktop
set -U XDG_DOWNLOAD_DIR $HOME/Desktop/download

set -U DEFAULT_USER d4n1
set -U EDITOR nvim
set -U TERM xterm-256color

if test "$XDG_VTNR" = 1
  if test "$DISPLAY" = ''
    sudo cryptsetup open /dev/sdb1 disk
    sudo sudo mount /dev/mapper/disk ~/Desktop/download
    enlightenment_start
  end
end

load_nvm > /dev/stderr

alias python=python3
alias n=nvim

neofetch --color_blocks off
```

[Fish](https://fishshell.com)
