title: Apt
date: 2000-4-4
category: Package

Advanced Package Tool, or APT, is a free-software user interface that works 
with core libraries to handle the installation and removal of software on 
Debian. 

APT simplifies the process of managing software on Unix-like computer systems
by automating the retrieval, configuration and installation of software 
packages, either from precompiled files or by compiling source code.

#### Init

```
mkdir sid
sudo debootstrap sid sid http://ftp.br.debian.org/debian
sudo chroot sid

echo "/proc proc/ proc defaults 0 0" >> /etc/fstab
mount /proc
apt update
apt install autopkgtest blhc devscripts dh-make dput-ng \
    how-can-i-help locales quilt renameutils spell \
    splitpatch tree wget vim

vim ~/.bashrc
export DEBFULLNAME="Daniel Pimentel"
export DEBEMAIL="d4n1@d4n1.org"
export EDITOR=vim
export QUILT_PATCHES=debian/patches
export QUILT_DIFF_ARGS="--no-timestamps --no-index -pab"
export QUILT_REFRESH_ARGS="--no-timestamps --no-index \
    -pab"

dpkg-reconfigure locales
dpkg-reconfigure tzdata

vim /etc/lintianrc
display-info = yes
pedantic = yes
display-experimental = yes
```

#### GPG

```
echo "pinentry-mode loopback" >>  ~/.gnupg/gpg.conf
gpg --full-gen-key
gpg --list-keys
gpg --import <publickey> <privatekey>

vim /etc/devscripts.conf
DEBSIGN_KEYID=<key>

```

#### Packaging

```
wget <upstreal-url>
tar -xvf upstream-1.0.tar.xz
cd upstream-1.0

licensecheck -r *
egrep -sriA25 '(copyright|public dom)'

dh_make -f ../upstream-1.0.tar.xz -c <license>

cd debian
mv compat <upstream>.* manpage.* menu.* post* pre* README.* watch* *.docs /tmp
vim changelog copyright control rules source/format tests/control watch

cd ..
debuild

lintian -i

uscan

blhc --all --debian <upstream.build>

debi

mkdir debian/test
vim debian/test/control
autopkgtest . -- null
```

# Fixing Lib Package

```
dpkg-deb -R package-1.0.0.deb /tmp/package-deb && sed -i 's/libappindicator1/libayatana-appindicator1/g' /tmp/package-deb/DEBIAN/control && dpkg-deb -b /tmp/package-deb package-fixed.deb
```

[Debian](https://www.debian.org)
