title: Bazzar
date: 2002-6-6
category: CVSD


Bazaar is a version control system that helps you track project
history over time and to collaborate easily with others. Whether
you're a single developer, a co-located team or a community of
developers scattered across the world, Bazaar scales and adapts to
meet your needs. Part of the GNU Project.


#### Install

```
sudo apt install bazzar
```

#### Add

```
touch foo
bzr add foo
```

### Commit

```
bzr commit -m 'commit message'
```

### Remove

```
bzr remove bar
```

### Log

```
bzr log
```

### Diff

```
bzr diff
```

### Branch

```
bzr branch development
```

### Merge

```
bzr merge
```

[GNU Bazaar](http://gnu.org/software/bazaar/)
