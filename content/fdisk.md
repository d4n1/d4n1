title: Fdisk
date: 2004-7-7
category: Utils

GNU Fdisk is used for creating, deleting, resizing, moving,
checking, and copying disk partitions and their file systems.

#### Install

```
sudo apt install fdisk
```

#### Run

```
fdisk /dev/sda
```

[Gnu Fdisk](http://gnu.org/software/fdisk/)

