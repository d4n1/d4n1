title: Scheme
date: 2006-2-2
category: Language

Scheme is a functional programming language and one of the two
main dialects of the programming language Lisp. Unlike Common Lisp,
the other main dialect, Scheme follows a minimalist design philosophy
specifying a small standard core with powerful tools for language
extension.

#### Run

```
guile

(define name "World")
(display (string-append "Hello " name "!"))
(newline)
```

[Scheme](http://schemers.org/)
