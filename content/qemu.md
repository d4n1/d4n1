title: Qemu
date: 2013-1-1
category: Emulator

QEMU (short for Quick Emulator) is a free and open-source hosted
hypervisor that performs hardware virtualization. QEMU is a hosted
virtual machine monitor: It emulates CPUs through dynamic binary
translation and provides a set of device models, enabling it to run a
variety of unmodified guest operating systems. It also can be used
together with KVM in order to run virtual machines at near-native
speed (requiring hardware virtualization extensions on x86
machines). QEMU can also be used purely for CPU emulation for
user-level processes, allowing applications compiled for one
architecture to be run on another.

#### Run

```
qemu-img create -f qcow2 image.img 5G 
qemu-system-i386 -hda image.img -cdrom image.iso -boot d -m 2047 -net nic  -net user -enable-kvm
qemu-system-i386 -hda image.img -m 2047 -enable-kvm -net nic -net user -soundhw ac97
```

[Qemu](http://www.qemu-project.org)
