title: Fdroid
date: 2016-3-3
category: Package

F-Droid is a robot with a passion for Free and Open
Source (FOSS) software on the Android platform. On this site you’ll
find a repository of FOSS apps, along with an Android client to
perform installations and updates, and news, reviews and other
features covering all things Android and software-freedom related.

[F-Droid](https://f-droid.org/)
