title: GTK
date: 2003-6-6
category: Graphic

GTK+, or the GIMP Toolkit, is a multi-platform toolkit for
creating graphical user interfaces. Offering a complete set of
widgets, GTK+ is suitable for projects ranging from small one-off
tools to complete application suites.
 
#### nvim ~/.gtkrc-2.0

```
gtk-theme-name='theme'
gtk-icon-theme-name='icon'
```

#### nvim ~/.config/gtk-3.0/settings.ini

```
[Settings]
gtk-theme-name=theme
gtk-icon-theme-name=icon
```

[GNU GTK](href "http://www.gtk.org/)
