title: Armory
date: 2013-3-3
category: Cryptocurrency


Armory is the most secure and full featured solution available
for users and institutions to generate and store Bitcoin private
keys. This means users never have to trust the Armory team. Satoshi
would be proud! Users are empowered with multiple encrypted Bitcoin
wallets and permanent one-time ‘paper backups’. Armory pioneered cold
storage and distributed multi-signature. Bitcoin cold storage is a
system for securely storing Bitcoins on a completely air-gapped
offline computer. The Armory team is highly experienced in
cryptography and private key ceremonies. For example, they have
collaborated with Verisign on developing an innovative Identity
Verification Specification for establishing trust on the Internet. At
Armory, we strive to constantly improve the best Bitcoin wallet with
new security features.

#### Run

```
armory
```

[Armory](http://www.bitcoinarmory.com/)
