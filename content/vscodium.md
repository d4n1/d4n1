title: VSCodium
date: 2018-12-12
category: Develop

VSCodium is a community-driven, freely-licensed binary distribution of Microsoft’s editor VS Code.

#### Install

```
sudo flatpak install vscodium
```

#### Sandbox

```
sudo flatpak override com.vscodium.codium --filesystem=host-os
```


#### Settings.json

```
{
    "workbench.colorTheme": "Default Dark+",
    "window.menuBarVisibility": "toggle",
    "workbench.colorCustomizations": {
        "statusBar.background": "#303030",
        "statusBar.noFolderBackground": "#222225",
        "statusBar.debuggingBackground": "#511f1f"
    },
    "vscode-pets.position": "explorer",
    "terminal.integrated.defaultProfile.linux": "bash",
    "terminal.integrated.profiles.linux": {
        "bash": {
          "path": "/usr/bin/flatpak-spawn",
          "args": ["--host", "--env=TERM=xterm-256color", "zsh"]
        }
    }
}
```

#### Extensions

- Debian
- Docker
- EsLint
- GitLens
- Go
- Import Cost
- JavaScript (ES6) code snippet
- Lua
- MongoDB
- Redis
- Rust
- Thunder Client
- Vscode-pets
- Isort
- Python
- Lua

[VSCodium](https://vscodium.com)
