title: Elisp
date: 2006-1-1
category: Language

Emacs Lisp is a dialect of the Lisp programming language used as
a scripting language by Emacs (a text editor family most commonly
associated with GNU Emacs and XEmacs). It is used for implementing
most of the editing functionality built into Emacs, the remainder
being written in C (as is the Lisp interpreter itself). Emacs Lisp is
also referred to as Elisp, although there is also an older, unrelated
Lisp dialect with that name.

#### Shell

```
emacs

(+ 4 4)

`C-x C-e'
```

[Elisp](https://gnu.org/software/emacs/manual/elisp.html)
