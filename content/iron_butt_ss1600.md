title: Iron Butt - Saddle Sore 1600 km
date: 2021-4-4
category: Motorcycle

I ride 1648 KM with Dark in less than 24 hours in my birthday!

#### Trip

  - Honda CB 500F 2018 (Dark)
  - 1648 KM
  - 24 hours

#### Certificate

![IronButt](http://www.d4n1.org/theme/images/post/iron-butt-ss1-cert.jpg)

This is SaddleSore by [Iron Butt Association](http://www.ironbutt.org).

