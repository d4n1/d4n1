title: Gzip
date: 2001-5-5
category: Utils

gzip reduces the size of the named files using Lempel–Ziv
coding (LZ77). Whenever possible, each file is replaced by one with
the extension ‘.gz’, while keeping the same ownership modes, access
and modification times. (The default extension is ‘-gz’ for VMS, ‘z’
for MSDOS, OS/2 FAT and Atari.) If no files are specified or if a file
name is -, the standard input is compressed to the standard
output. gzip will only attempt to compress regular files. In
particular, it will ignore symbolic links.

#### Compress

```
gzip -c file1 file2 > foo.gz
```

#### Descompress

```
gzip -d foo.gz
gunzip foo.gz
```

[GNU Gzip](http://www.gnu.org/software/gzip/)
