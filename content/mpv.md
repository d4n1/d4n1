title: Mpv
date: 2012-1-1
category: Video

Mpv is media player software, based on MPlayer and mplayer2. It
is free software released under a mix of licenses including GNU
General Public License version 2 plus (GPLv2+), with parts under GNU
Lesser Public License version 2.1 plus (LGPLv2.1+), and some optional
parts under GNU General Public License version 3 (GPLv3).

#### Shell

```
mpv video.ogv
```

[Mpv](https://mpv.io)
