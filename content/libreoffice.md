title: Libreoffice
date: 2012-11-11
category: office

LibreOffice is a free and open source office suite, a project of
The Document Foundation. It was forked from OpenOffice.org in 2010,
which was an open-sourced version of the earlier StarOffice. The
LibreOffice suite comprises programs for word processing, the creation
and editing of spreadsheets, slideshows, diagrams and drawings,
working with databases, and composing mathematical formulae. It is
available in 110 languages.

[Libreoffice](http://www.libreoffice.org/)
