title: Scribus
date: 2012-12-12
category: Graphic

Scribus is a desktop publishing (DTP) application, released under
the GNU General Public License as free software. Scribus is designed
for layout, typesetting, and preparation of files for
professional-quality image-setting equipment. It can also create
animated and interactive PDF presentations and forms. Example uses
include writing newspapers, brochures, newsletters, posters, and
books.

[Scribus](https://www.scribus.net/)
