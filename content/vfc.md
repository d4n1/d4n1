title: VFC
date: 2019-6-6
category: Motorcycle

VFC (Valente Fazedor de Chuva in portuguese, Brave Rainmaker in english) is a motorcycle challenge where the rider goes arround all the cities in his state, in my case 102 cities in Maceió - AL, Brazil.

#### Trip

- Honda CB 500F 2018 (Dark)
- 102 cities
- 4466 km
- 13 days

#### Certificate

![VFC-AL](http://www.d4n1.org/theme/images/post/vfc.jpg)

[VFC](http://www.fazedoresdechuva.com/forums/showthread.php/3341-Hacking-AL102)
