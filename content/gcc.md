title: GCC
date: 2000-9-9
category: Compiler

The GNU Compiler Collection includes front ends for C, C++,
Objective-C, Fortran, Java, Ada, and Go, as well as libraries for
these languages (libstdc++, libgcj,...). GCC was originally written as
the compiler for the GNU operating system. The GNU system was
developed to be 100% free software, free in the sense that it respects
the user's freedom.

#### Languages
    
The standard compiler include front ends for C (gcc), C++ (g++),
Objective-C, Objective-C++, Fortran (gfortran), Java (gcj),
Ada (GNAT), and Go (gccgo).

[GNU GCC](http://gcc.gnu.org/)
