title: Grep
date: 2001-7-7
category: Utils

Grep searches input files for lines containing a match to a
given pattern list. When it finds a match in a line, it copies the
line to standard output (by default), or produces whatever other sort
of output you have requested with options.

#### Search

```
grep -r 'grep' .
```

[GNU Grep](http://www.gnu.org/software/grep/)
