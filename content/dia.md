title: Dia
date: 2011-4-4
category: Graphic

Dia is roughly inspired by the commercial Windows program
'Visio,' though more geared towards informal diagrams for casual
use. It can be used to draw many different kinds of diagrams. It
currently has special objects to help draw entity relationship
diagrams, UML diagrams, flowcharts, network diagrams, and many other
diagrams. It is also possible to add support for new shapes by writing
simple XML files, using a subset of SVG to draw the shape.

[GNU Dia](https://wiki.gnome.org/Apps/Dia)

