title: TeXlive
date: 2012-4-4
category: Utils

TeXLive is a free software distribution for the TeX typesetting
system that includes major TeX-related programs, macro packages, and
fonts. It is the replacement of its no-longer supported counterpart
teTeX.

#### Run

```
pdflatex file.tex
bibtex file.bib
```

[TeXLive](https://www.tug.org/texlive)
