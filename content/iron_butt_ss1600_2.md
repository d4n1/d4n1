title: Iron Butt - Saddle Sore 1000 miles
date: 2022-4-4
category: Motorcycle

I ride 1648 KM with Funérea in less than 24 hours from Capital Moto Week on Brasilia to Maceió!

#### Trip

  - Yamaha MT-07 2016 (Funérea)
  - 1648 KM
  - 24 hours

#### Certificate

![IronButt](http://www.d4n1.org/theme/images/post/iron-butt-ss2-cert.jpg)

This is SaddleSore by [Iron Butt Association](http://www.ironbutt.org).
