title: Electrum
date: 2013-2-2
category: Cryptocurrency

Electrum is a lightweight Bitcoin client, based on a
client-server protocol. It supports Simple Payment Verification (SPV)
and deterministic key generation from a seed.  Your secret keys are
encrypted and are never sent to other machines/servers.  Electrum does
not download the Bitcoin blockchain.

#### Install

```
sudo apt install electrum
```

### Run

```
electrum
```

[Electrum](http://www.qemu-project.org)
