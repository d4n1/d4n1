title: Parted
date: 2004-8-8
category: Utils

GNU Parted manipulates partition tables. This is useful for
creating space for new operating systems, reorganizing disk usage,
copying data on hard disks and disk imaging. The package contains a
library, libparted, as well as well as a command-line frontend,
parted, which can also be used in scripts.

#### Run

```
parted /dev/sda
mklabel gpt
mkpart primary ext4 0% 512MiB
mkpart primary linux-swap 512MiB 8GiB
mkpart primary ext3 8GiB 100%
set 1 bios_grub on
```

[GNU Parted](http://gnu.org/software/parted/)
