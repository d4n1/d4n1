title: Nano
date: 2003-2-2
category: Editor

GNU nano is a small and friendly text editor. Besides basic text
editing, nano offers many extra features, such as an interactive
search-and-replace, undo/redo, syntax coloring, smooth scrolling,
auto-indentation, go-to-line-and-column-number, feature toggles, file
locking, backup files, and internationalization support.

[Nano](http://www.gnu.org/software/nano/)
