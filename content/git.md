title: Git
date: 2002-7-7
category: CVSD

Git is a distributed version-control system for tracking changes in source
code during software development. It is designed for coordinating work 
among programmers, but it can be used to track changes in any set of
files. Its goals include speed, data integrity, and support for 
distributed, non-linear workflows.

#### Install

```
sudo apt install git
```

#### Clone

```
git clone <git@d4n1.org.git>
```

#### Add

```
touch foo bar
git add .
```

#### Commit

```
git commit -m 'commit message'
```

#### Push

```
git push origin master
```

#### Pull

```
git pull origin master
```

#### Log

```
git log
```

#### Diff

```
git diff
```

#### Branch

```
git branch development
```

#### Checkout

```
git checkout development
```


#### Merge

```
git checkout master
git merge development
```

#### Tag

```
git tag -a v1.0 -m 'Foobar release'
```

#### Remote

```
git remote -v
git remote add upstream <git@d4n1.org.git>
git fetch upstream
git merge upstream/master
git push
```

[Git](https://git-scm.com/)
