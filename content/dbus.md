title: Dbus
date: 2014-9-9
category: Net

D-Bus is a message bus system, a simple way for applications to talk to one another. 
In addition to interprocess communication, D-Bus helps coordinate process lifecycle; 
it makes it simple and reliable to code a 'single instance' application or daemon, and
to launch applications and daemons on demand when their services are needed.

[Dbus](https://www.freedesktop.org/wiki/Software/dbus/)
