title: Firefox
date: 2000-8-8
category: Browser

Mozilla Firefox (or simply Firefox) is a free and open-source web browser 
developed by Mozilla Foundation and its subsidiary, Mozilla Corporation. 
Firefox uses the Gecko layout engine to render web pages, which 
implements current and anticipated web standards.
In 2016, Firefox began incorporating new technology under the code name
Quantum to promote parallelism and a more intuitive user interface.

Firefox was created in 2002 under the codename "Phoenix" by the Mozilla 
community members who desired a standalone browser, rather than the Mozilla
Application Suite bundle. 
During its beta phase, Firefox proved to be popular with its testers and was
praised for its speed, security, and add-ons. 

[Firefox](https://www.mozilla.org/en-US/firefox/)
