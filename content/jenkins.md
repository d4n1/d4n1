title: Jenkins
date: 2019-8-8
category: CI

The leading open source automation server, Jenkins provides hundreds of 
plugins to support building, deploying and automating any project.

#### Install

```
sudo apt install default-jre default-jdk apt-transport-https wget gnupg
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo apt update
sudo apt install jenkins
```

#### Config

```
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

Access http://ip:8080.

[Jennkins](https://jenkins.io)
