title: RFC BR-116
date: 2018-2-2
category: Motorcycle

RFC (Rodoviário Fazedor de Chuva in portuguese, Road Rainmaker in english) is a motorcycle challenge where the rider travels along the entire BR-116 highway in Brazil, called Death's highway.

#### Trip

- Honda CB 500F 2018 (Dark)
- 4488 km
- 13 days

#### Route

![RFC-BR-116](http://www.d4n1.org/theme/images/post/rfc-br-116.jpg)

#### Certificate

![RFC-BR-116](http://www.d4n1.org/theme/images/post/rfc-br-116-cert.jpg)

[RFC](http://www.fazedoresdechuva.com/forums/showthread.php/3600-Hacking-BR)
