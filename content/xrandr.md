title: XrandR
date: 2014-5-5
category: Utils

RandR ('resize and rotate') is a communications protocol written
as an extension to the X11 protocol. XRandR provides the ability to
resize, rotate and reflect the root window of a screen. RandR is also
responsible for setting the screen refresh rate.

#### Run

```
xrandr --output HDMI1 -mode 1360x768
```

[XRandR](http://www.x.org/wiki/Projects/XRandR/)

