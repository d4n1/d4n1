title: Gimp
date: 2001-1-1
category: Graphic

GIMP is an acronym for GNU Image Manipulation Program. It is a
freely distributed program for such tasks as photo retouching, image
composition and image authoring. The terms of usage and rules about
copying are clearly listed in the GNU General Public License.

#### Programming Algorithms

GIMP is a high quality framework for scripted image
manipulation, with multi-language support such as C, C++, Perl,
Python, Scheme, and more!

[GNU Gimp](https://www.gimp.org/)
