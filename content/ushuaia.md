title: Ushuaia
date: 2022-11-11
category: Motorcycle

I ride 18000 KM with Funérea from Maceió in Brazil to Ushuaia in Argentina.

#### Trip

  - Yamaha MT-07 2016 (Funérea)
  - 18000 KM

![Funérea and me](http://www.d4n1.org/theme/images/post/ushuaia.jpg)

Upon arrival in Rio Grande on Tierra del Fuego, I went to Ushuaia and returned to Rio Grande. I was
awarded the certificate "Motero Exemplar" from La Comunidade Internacional Motero Exemplar on Ushuaia.

![Motero Exemplar certificate](http://www.d4n1.org/theme/images/post/ushuaia1.jpg)
![Motero Exemplar certificate](http://www.d4n1.org/theme/images/post/ushuaia2.jpg)


