title: LGPL
date: 2008-3-3
category: License

The GNU Lesser General Public License (LGPL) is a free software
license published by the Free Software Foundation (FSF). The license
allows developers and companies to use and integrate software released
under the LGPL into their own (even proprietary) software without
being required by the terms of a strong copyleft license to release
the source code of their own components. The license only requires
software under the LGPL be modifiable by end users via source code
availability. For proprietary software, code under the LGPL is usually
used in the form of a shared library such as a DLL, so that there is a
clear separation between the proprietary and LGPL components. The LGPL
is primarily used for software libraries, although it is also used by
some stand-alone applications. The license was originally called the
GNU Library General Public License and was first published in 1991,
and adopted the version number 2 for parity with GPL version 2. The
LGPL was revised in minor ways in the 2.1 point release, published in
1999, when it was renamed the GNU Lesser General Public License to
reflect the FSF's position that not all libraries should use
it. Version 3 of the LGPL was published in 2007 as a list of
additional permissions applied to GPL version 3.

[GNU LGPL](https://www.gnu.org/licenses/lgpl.html)
