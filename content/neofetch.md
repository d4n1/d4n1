title: Neofetch
date: 2020-2-2
category: Utils

Neoftech is a cross-platform and easy-to-use system information
command line script that collects your Linux system information
and display it on the terminal next to an image, it could be your
distributions logo or any ascii art of your choice

#### Install

```
sudo apt install neofetch
```

#### Run

```
neofetch
```

[Neofetch](https://github.com/dylanaraps/neofetch)
