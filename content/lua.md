title: Lua
date: 2005-05-05
category: Language

Lua was originally designed in 1993 as a language for extending
software applications to meet the increasing demand for customization
at the time. It provided the basic facilities of most procedural
programming languages, but more complicated or domain-specific
features were not included; rather, it included mechanisms for
extending the language, allowing programmers to implement such
features. As Lua was intended to be a general embeddable extension
language, the designers of Lua focused on improving its speed,
portability, extensibility, and ease-of-use in development.

#### lua

```
print("Hello, world!")
```

[Lua](href "http://lua.org)
