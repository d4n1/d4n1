title: Steam
date: 2018-4-4
category: Games

Steam is a digital distribution platform developed by Valve Corporation for
purchasing and playing video games. Steam offers digital rights management 
(DRM), matchmaking servers, video streaming, and social networking services. 
Steam provides the user with installation and automatic updating of games, 
and community features such as friends lists and groups, cloud saving, 
and in-game voice and chat functionality.

#### Install

```
vim /etc/apt/source.list 

deb http://ftp.br.debian.org/debian/ sid main non-free contrib
deb-src http://ftp.br.debian.org/debian/ sid main non-free contrib

dpkg --add-architecture i386

apt update

apt install steam libgl1-nvidia-glx:i386
```

#### Usage

```
optirun steam
```

#### Steam Controller

```
vim /etc/udev/rules.d/99-steam-controller-perms.rules

#Valve USB devices
SUBSYSTEM=="usb", ATTRS{idVendor}=="28de", TAG+="uaccess", TAG+="udev-acl"

# Steam Controller udev write access
KERNEL=="uinput", SUBSYSTEM=="misc", TAG+="uaccess", TAG+="udev-acl"

# Valve HID devices over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="28de", TAG+="uaccess", TAG+="udev-acl"

# Valve HID devices over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*28DE:*", TAG+="uaccess", TAG+="udev-acl"

# DualShock 4 over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="05c4", TAG+="uaccess", TAG+="udev-acl"

# DualShock 4 wireless adapter over USB hidraw
KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="0ba0", TAG+="uaccess", TAG+="udev-acl"

# DualShock 4 Slim over USB hidraw

KERNEL=="hidraw*", ATTRS{idVendor}=="054c", ATTRS{idProduct}=="09cc", TAG+="uaccess", TAG+="udev-acl"

# DualShock 4 over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*054C:05C4*", TAG+="uaccess", TAG+="udev-acl"

# DualShock 4 Slim over bluetooth hidraw
KERNEL=="hidraw*", KERNELS=="*054C:09CC*", TAG+="uaccess", TAG+="udev-acl"
```

#### Launch Options

```
`pvkrun %command%`
```

[Steam](https://steamcommunity.com/)

