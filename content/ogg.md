title: Ogg
date: 2002-8-8
category: Video

Ogg is free professional-grade media format endorsed by FSF. Ogg
Vorbis encodes audio and Ogg Theora encodes video. When you see a file
with the Ogg extension play it!

#### Run

```
ffmpeg -i input.mp3 output.ogg
ffmpeg -i input.mkv -q:a 10 -q:v 10 output.ogg
```

[Ogg](https://www.fsf.org/campaigns/playogg/en)
