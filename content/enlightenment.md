title: Enlightenment
date: 2000-2-2
category: Desktop

Enlightenment was launched in the 1990s by Carsten "Rasterman" Haitzler as an easy to use Window Manager (WM) for X11. Since then it has expanded to include the one million lines of C code that form the Enlightenment Foundation Libraries (EFL) and a diverse set of applications. There's a vibrant and active community of developers and users who work on and use the code every day.

Enlightenment is classed as a "desktop shell" as it provides everything you need to operate your desktop or laptop, but it is not a full application suite. This covers functionality including launching applications, managing their windows and performing system tasks like suspending, rebooting, managing files and so on.

#### Install

```
apt install enlightenment connman-ui
```

#### Touchpad

```
nvim /etc/X11/xorg.conf.d/30-touchpad.conf
Section "InputClass"
    Identifier "MyTouchpad"
    MatchIsTouchpad "on"
    Driver "libinput"
    Option "Tapping" "on"
EndSection
```

[Enlightenment](https://www.enlightenment.org/)
