title: LilyPond
date: 2015-08-08
category: Audio

LilyPond is a music engraving program, devoted to producing the
highest-quality sheet music possible. It brings the aesthetics of
traditionally engraved music to computer printouts. LilyPond is free
software and part of the GNU Project.

[Lilypond](http://gnu.org/software/lilypond)
