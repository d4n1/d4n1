title: Haskell
date: 2005-06-06
category: Language

Haskell is a standardized, general-purpose purely functional
programming language, with non-strict semantics and strong static
typing. It is named after logician Haskell Curry. The latest standard
of Haskell is Haskell 2010. As of May 2016, a group is working on the
next version, Haskell 2020.

#### ghc

```
main = putStrLn \"Hello World\"
```

[Haskell](http://haskell.org)
