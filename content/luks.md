title: Luks
date: 2004-2-2
category: Crypt

The Linux Unified Key Setup (LUKS) is a disk encryption specification 
created by Clemens Fruhwirth in 2004 and was originally intended for Linux.

#### Install

```
sudo apt install cryptsetup
```

#### Encrypt

```
sudo cryptsetup luksFormat /dev/sdb1
sudo cryptsetup open /dev/sdb1 disk
sudo mkfs.ext4 /dev/mapper/disk
```

#### (Un)Mount

```
sudo cryptsetup --type luks open /dev/sdb1 disk
sudo mount -t ext4 /dev/mapper/disk /disk

sudo umount /disk
sudo cryptsetup close disk
```

#### Automount

```
sudo vim /etc/fstab
/dev/mapper/disk /disk ext4 defaults 0 0

sudo blkid /dev/mapper/disk

sudo vim /etc/crypttab
disk /dev/sdb1 none luks

```

[Luks](https://gitlab.com/cryptsetup/cryptsetup/)
