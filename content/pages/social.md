title: Social
date: 2022-4-4
category: social

Follow me:

- [Mastodon](https://mastodon.social/@d4n1)
- [X](https://x.com/4k4d4n1)
- [Instagram](https://www.instagram.com/4k4d4n1/)
- [Linkedin](https://www.linkedin.com/in/daniel-pimentel-6965861b9/)
- [Youtube](https://www.youtube.com/@d4n1)
- [Steam](https://steamcommunity.com/id/4k4d4n1/)
- **Play Station** @d4n1_4k4
- **Nitendo**: 4k4d4n1
