title: Contact
date: 2021-4-4 
category: contact

Send me an email ([d4n1@d4n1.org](mailto:d4n1@d4n1.org)) and assign my key:

```
pub   rsa4096 2000-01-01 [SC]
      D9F9 EADD 6F26 AC78 2CFC  D831 7FEC E904 2C7B FD45
uid           [ultimate] Daniel Pimentel <d4n1@d4n1.org>
sub   rsa4096 2000-01-01 [E]
```

Send me a message on Matrix (@d4n1), Signal or Telegram (@d4n14k4).
