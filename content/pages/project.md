title: Project
date: 2021-4-4
category: project

Follow my projects:

- [Gitlab](https://gitlab.com/d4n1)
- [Debian](https://salsa.debian.org/d4n1)
- [GNU](https://savannah.gnu.org/users/d4n1) 
- [Github](https://github.com/d4n1)

Make a donation:

- bc1q4965lmwcx6vmer4z8ha89rmm9jxek5w35r45rd (BTC)
- 0x8E7d640764e7f4Cc12188E5EE1CF7d3F1E758d86 (ETH)
