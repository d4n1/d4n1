title: About
date: 2021-4-4
category: about

My name is Daniel (a.k.a d4n1).

Master in Computing, Specialist in Software Engineering, Graduate in Analysis and Development Systems, Technical in Computing, Developer, Guitarist, Motorcyclist and Geek.

[Contact](https://www.d4n1.org/pages/contact.html) me, follow my [Projects](https://www.d4n1.org/pages/project.html) and
my [Social Network](https://www.d4n1.org/pages/social.html) or see my [CV]( https://lattes.cnpq.br/3807423183790142).
