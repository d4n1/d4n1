title: OpenSSH
date: 2012-10-10
category: Net

OpenSSH (also known as OpenBSD Secure Shell) is a suite of
security-related network-level utilities based on the Secure
Shell (SSH) protocol, which help to secure network communications via
the encryption of network traffic over multiple authentication methods
and by providing secure tunneling capabilities.

#### Run

```
ssh user@domain
```

[OpenSSH](http://www.openssh.org/)
