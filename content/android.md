title: Android
date: 2008-12-12
category: Operational System

Android is a mobile operating system based on a modified version of the Linux kernel and other open-source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google. It was unveiled in November 2007, with the first commercial Android device, the HTC Dream, being launched in September 2008.


#### Install

```
apt install fastboot adb
```

#### Boot

```
fastboot reboot
fastboot boot <boot>
```

#### Recovery

```
fastboot flash recovery <recovery>
```

#### Push

```
adb push <package> <path>
```


#### Sideload

```
adb sideload <package>
```

[Android](https://android.com/)

