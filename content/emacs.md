title: Emacs
date: 2000-7-7
category: Editor

GNU Emacs was originally implemented in 1976 on the MIT AI Lab's
Incompatible Timesharing System (ITS), as a collection of TECO
macros. The name “Emacs” was originally chosen as an abbreviation of
'Editor MACroS'. This version of Emacs, GNU Emacs, was originally
written in 1984 in Lisp under GPL license.

#### Shell

```
M-x shell
M-x eshell
```

#### Compile

```
M-x compile
```

#### Debug

```
M-x gdb
```

#### Grep

```
M-x grep
```

#### Man

```
M-x man
```

#### Diff

```
M-x ediff
```

#### FIle manager

```
M-x dired
```

#### Read news, mail and feeds

```
M-x gnus
```

#### Games

```
M-x doctor
M-x tetris
M-x snake
M-x 5x5
M-x blackbox
M-x decipher
M-x dissociated-press
M-x dunnet
M-x hanoi
M-x landmark
M-x life
M-x morse-region
M-x mpuz
M-x pong
M-x snake
M-x solitaire
M-x studlify-region
M-x zone
```

#### IRC

```
M-x irc
```

#### Invoking shell commands

```
M-x shell-command or M-!
```

#### Tools

```
M-x calculator
M-x calendar
M-x phases-of-moon
```

#### Version control

```
M-x vc-next-action or C-x v v
```

#### Help

```
C-h t
C-h r
C-h C-f
```

#### Moving in buffers

Key | Meaning 
----| -------
C-f | Forward one character
C-n | Next line
C-b | Back one character
C-p | Previous line
C-a | Beginning of line
M-f | Forward one word
M-a | Previous sentence
M-v | Previous screen
M-< | Beginning of buffer
C-e | End of line
M-b | Back one word
M-e | Next sentence
C-v | Next screen
M-> | End of buffer
C-u 3 C-p | Back 3 lines
C-u 10 C-f | Forward 10 characters
M-1 M-0 C-f | Forward 10 characters
C-u C-n	| Forward 4 lines
C-u C-u C-n | Forward 16 lines
C-u C-u C-u C-n	| Forward 64 lines
M-g g | Jump to specified line
C-s | Incremental search forward
C-r | Incremental search backward

#### Mark

Key | Meaning
----|--------
C-SPC |	Set mark to the current location
C-x C-x | Swap point and mark
C-u C-SPC | Cycle through mark ring

#### Region

Key | Meaning
----|--------
C-x h | Make region contain the entire buffer ('Select all')
M-h | Make region contain the current paragraph
C-x n n | Narrow buffer to the current region
C-x n w | Restore ('widen') buffer

#### Killing ('cutting') text

Key | Meaning
----|--------
C-k | Kill line
C-u 10 C-k | Kill 10 lines
C-w | Kill region ('cut')
M-w | Save region to kill ring without deleting ('copy')
M-d | Kill next word
M-k | Kill to end of sentence

#### Yanking ('pasting') text

Key | Meaning
----|--------
C-y ! Yanks last killed text
M-y | Replace yanked text with previously killed text

#### Undo

Key | Meaning
----|--------
C-/ | Undo
C-_ | Undo
C-x u | Undo

#### Incremental search

Key | Meaning
----|--------
C-s | Incremental search
C-s C-s	| Search for most recently searched item
C-s M-p	| Previous item in search history
C-s M-n	| Next item in search history
C-h k C-s | Guide to more commands available in incremental search
C-r | Backward incremental search

#### Search and replacement

Key | Meaning
----|--------
M-% | Query replace

#### Add symbols

Key | Meaning
----|--------
C-x 8 C-h | Help to add symbols

#### Regular expression search

Key | Meaning
----|--------
C-M-s | Regular expression incremental search

#### Keyboard Macros

Key | Meaning
----|--------
F3 | Start recording macro
F4 | Stop recording macro
F4 | Play back macro once
M-5 F4 | Play back macro 5 times
M-0 F4 | Play back macro over and over until it fails


#### My ~/.emacs.d/init.el

```
;;; Emacs is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Emacs is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Emacs.  If not, see <http://www.gnu.org/licenses/>.
;;;
;;; Emacs --- Emacs config
;;; Copyright 2000-2018 Daniel Pimentel <d4n1@d4n1.org>
;;;
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Emacs.  If not, see <http://www.gnu.org/licenses/>.

(package-initialize)

;; custom
(custom-set-variables
 '(inhibit-startup-screen t)
 '(tool-bar-mode nil)
 '(custom-enabled-themes (quote (wombat)))
 '(horizontal-scroll-bar-mode nil)
 '(scroll-bar-mode nil)
 '(column-number-mode t)
 )
(custom-set-faces
 '(cursor ((t (:background "red"))))
 )

;; melpa
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; org-mode
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
```

[GNU Emacs](https://www.gnu.org/software/emacs/)
